import React, { useState, useEffect, useMemo } from "react";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import "./DropFiles.css";

import * as FeatherIcons from "react-icons/fi";

const DropFiles = (props) => {
  const [files, setFiles] = useState([]);
  const [message, setMessage] = useState("");
  const [thumbs, setThumbs] = useState(null);

  useEffect(() => {
    //component did mount
    showMessage();
    // returned function will be called on component unmount
    return () => {
      let newFiles = Object.assign([], files);
      newFiles.forEach((file) => URL.revokeObjectURL(file.preview));
    };
  }, []);

  useMemo(() => {
    setFiles(props.listFiles);
  }, [props]);

  // Valida arquivos
  const checkDrop = (isDragAccept, isDragReject, list) => {
    let classValue = "";

    if (isDragAccept) classValue = "accept";
    if (isDragReject) classValue = "decline";
    if (list && list.length) classValue = "with-file";

    return classValue;
  };

  // Pega os arquivos selecionados
  const selectFile = (fileList) => {
    props.handleFile(fileList);
  };

  const showMessage = () => {
    // Verifica se o navegador suporta o FileReader
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      setMessage("");
    } else {
      setMessage("As APIs de arquivo não são suportadas pelo o seu navegador");
    }
  };

  return (
    <>
      <Dropzone {...props} onDropAccepted={selectFile}>
        {({ getRootProps, getInputProps, isDragAccept, isDragReject }) => (
          <div
            {...getRootProps({
              className: `dropzone ${checkDrop(
                isDragAccept,
                isDragReject,
                thumbs
              )}`,
            })}
          >
            <input {...getInputProps()} />

            {/* Preview Image */}
            {!props.multiple && files && files.length ? (
              files
                .filter((file) => file && file.preview)
                .map((file, index) => {
                  return (
                    <div className="center" key={file.name + index}>
                      {FeatherIcons ? (
                        <FeatherIcons.FiXCircle
                          className="remove"
                          onClick={(e) => props.remove(e)}
                          title="Remover imagem"
                        />
                      ) : (
                        <span
                          className="material-icons remove"
                          onClick={(e) => props.remove(e)}
                          title="Remover imagem"
                        >
                          close
                        </span>
                      )}

                      {file.type === "application/pdf" ? (
                        FeatherIcons ? (
                          <FeatherIcons.FiFileText size="24" />
                        ) : (
                          <span className="material-icons pdf">
                            picture_as_pdf
                          </span>
                        )
                      ) : (
                        <img
                          className="preview"
                          src={file.preview}
                          title={file.name}
                          alt="preview"
                        />
                      )}
                    </div>
                  );
                })
            ) : FeatherIcons ? (
              <FeatherIcons.FiUploadCloud size="40" />
            ) : (
              <span className="material-icons size-48">cloud_upload</span>
            )}

            {/* Texts */}
            {!isDragReject && (
              <span className="center-t margin-left" style={{ width: "100%" }}>
                {!props.loading ? props.title : "Carregando..."}
              </span>
            )}
            {isDragReject && (
              <span className="center-t margin-left text-danger">
                Os formatos de arquivos aceitos são PNG, JPG, JPEG
              </span>
            )}
          </div>
        )}
      </Dropzone>
      <span className="thumbLabel">Click sobre para remover o arquivo</span>
      {props.multiple && (
        <aside className="thumbsContainer">
          {files && files.length
            ? files.map((file, index) => (
                <div
                  className="thumb"
                  key={`${file.name}${index}`}
                  title={file.name}
                >
                  <div className="thumbInner">
                    {FeatherIcons ? (
                      <FeatherIcons.FiXCircle
                        size="24"
                        onClick={(e) => props.remove(e)}
                        title="Remover imagem"
                      />
                    ) : (
                      <span
                        className="material-icons remove"
                        onClick={(e) => props.remove(e)}
                        title="Remover imagem"
                      >
                        close
                      </span>
                    )}

                    {file.type === "application/pdf" ? (
                      FeatherIcons ? (
                        <FeatherIcons.FiFileText size="24" />
                      ) : (
                        <span className="material-icons pdf">
                          picture_as_pdf
                        </span>
                      )
                    ) : (
                      <img
                        className="preview"
                        src={file.preview}
                        alt="preview"
                        title={file.name}
                      />
                    )}
                  </div>
                </div>
              ))
            : null}
        </aside>
      )}

      {message && <p className="text-danger">{message}</p>}
    </>
  );
};

DropFiles.propTypes = {
  title: PropTypes.string,
  multiple: PropTypes.bool,
};

export default DropFiles;
