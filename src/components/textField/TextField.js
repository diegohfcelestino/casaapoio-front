import PropTypes from "prop-types";
import React from "react";
import { FormGroup, ControlLabel, FormControl } from "rsuite";

const TextField = (props) => {
  const { label, name, accepter, error, margin, width, textarea } = props;
  return (
    <>
      <div style={{ width: width ?? 300, margin: margin ?? 8 }}>
        <FormGroup>
          <ControlLabel>{label}</ControlLabel>
          <FormControl
            className="formInput"
            name={name}
            componentClass={textarea ? "textarea" : "input"}
            style={{ resize: textarea ? "auto" : "none", width: width ?? 300 }}
            accepter={accepter}
            errorMessage={error}
            {...props}
          />
        </FormGroup>
      </div>
    </>
  );
};

TextField.propTypes = {
  accepter: PropTypes.any,
  error: PropTypes.any,
  label: PropTypes.any,
  name: PropTypes.any,
};

export default TextField;
