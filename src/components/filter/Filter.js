import React, { useState } from "react";
import PropTypes from "prop-types";
import { Button } from "rsuite";
import { FiSearch, FiX } from "react-icons/fi";

import "./Filter.css";
import Autocomplete from "../form/autocomplete/Autocomplete";

const Filter = ({
  label,
  options,
  optionValue,
  optionName,
  value,
  loading,
  placeholder,
  onSearch,
  onSelect,
  onClean,
}) => {
  const [show, setShow] = useState(false);
  const [clickShow, setClickShow] = useState(false);
  return (
    <div className="filterMain">
      <Button
        appearance="ghost"
        active
        onClick={() => {
          setShow(!show);
          setClickShow(true);
        }}
        className="filterButton"
      >
        {show ? <FiX size="20" /> : <FiSearch size="20" />}
      </Button>
      <div
        className={`filterContainer ${
          clickShow && show ? "filterShow" : clickShow && "filterHide"
        }`}
      >
        <div style={show ? { display: "block" } : { display: "none" }}>
          <h3 className="filterTitle">Filtro/Pesquisa</h3>
          <Autocomplete
            label={label}
            options={options}
            optionValue={optionValue}
            optionName={optionName}
            value={value}
            loading={loading}
            placeholder={placeholder}
            onSearch={(value) => onSearch(value)}
            onSelect={(value, item) => {
              onSelect(value, item);
            }}
            onClean={() => onClean()}
          />
        </div>
      </div>
    </div>
  );
};

Filter.propTypes = {
  label: PropTypes.any,
  loading: PropTypes.any,
  onClean: PropTypes.func,
  onSearch: PropTypes.func,
  onSelect: PropTypes.func,
  optionName: PropTypes.any,
  optionValue: PropTypes.any,
  options: PropTypes.any,
  placeholder: PropTypes.any,
  value: PropTypes.any,
};

export default Filter;
