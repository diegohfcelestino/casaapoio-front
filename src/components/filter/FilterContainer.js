import React, { useState } from "react";
import PropTypes from "prop-types";
import { Button } from "rsuite";
import { FiSearch, FiX } from "react-icons/fi";

import "./Filter.css";

const Filter = ({ children }) => {
  const [show, setShow] = useState(false);
  const [clickShow, setClickShow] = useState(false);
  return (
    <div className="filterMain">
      <Button
        appearance="ghost"
        active
        onClick={() => {
          setShow(!show);
          setClickShow(true);
        }}
        className="filterButton"
      >
        {show ? <FiX size="20" /> : <FiSearch size="20" />}
      </Button>
      <div
        className={`filterContainer ${
          clickShow && show ? "filterShow" : clickShow && "filterHide"
        }`}
        style={show ? { display: "block" } : { display: "none" }}
      >
        <div style={show ? { display: "block" } : { display: "none" }}>
          <h3 className="filterTitle">Filtro/Pesquisa</h3>
          {children}
        </div>
      </div>
    </div>
  );
};

Filter.propTypes = {
  children: PropTypes.any,
};

export default Filter;
