import PropTypes from "prop-types";
import React from "react";

import "../Form.css";
import { FormGroup, ControlLabel, DateRangePicker } from "rsuite";
import removerAcentosEspacos from "../../../services/removerAcentos";

const DateRangeInput = ({
  label,
  form,
  value,
  setForm,
  invalid,
  error,
  state,
}) => {
  return (
    <>
      <FormGroup className="formFormGroup">
        <ControlLabel>{label}</ControlLabel>
        <DateRangePicker
          format="DD/MM/YYYY"
          name={removerAcentosEspacos(label)}
          locale={{
            sunday: "Do",
            monday: "Se",
            tuesday: "Te",
            wednesday: "Qa",
            thursday: "Qi",
            friday: "Sx",
            saturday: "Sa",
            ok: "Confirmar",
            today: "Hoje",
            yesterday: "Ontem",
            last7Days: "7 dias atrás",
          }}
          className="formInput"
          componentClass="input"
          value={value}
          onChange={(value) =>
            setForm({
              ...form,
              [state]: value,
            })
          }
        />
        {invalid && <p className="formInputError">{error}</p>}
      </FormGroup>
    </>
  );
};

DateRangeInput.propTypes = {
  error: PropTypes.any,
  form: PropTypes.any,
  invalid: PropTypes.any,
  label: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
  value: PropTypes.any,
};

export default DateRangeInput;
