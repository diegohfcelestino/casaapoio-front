import React from "react";
import { FormGroup, ControlLabel } from "rsuite";
import { Lookup, DropDownOptions } from "devextreme-react/lookup";
import DataSource from "devextreme/data/data_source";
import { getCidadeAutoComplete } from "../../../store/modules/entidades/action";

const SmartCombo = ({
  autoFocus,
  label,
  disabled,
  form,
  setForm,
  invalid,
  error,
  state,
  stateBlur,
  placeholder,
  options,
  value,
  labelKey,
  valueKey,
  items,
  onSelect,
  defaultValue,
  dataSource,
  setsFormsCidade,
}) => {
  // const groupedData = new DataSource({
  //   load: getCidadeAutoComplete,
  //   paginate: true,
  //   pageSize: 5,
  // });

  return (
    <>
      <FormGroup className="formFormGroup">
        <ControlLabel>{label}</ControlLabel>
        <Lookup
          paginate={true}
          placeholder={placeholder}
          dataSource={dataSource}
          pageLoadMode="nextButton"
          nextButtonText="More"
          onSelectionChanged={(events) => setsFormsCidade(events)}
          className="selectInputContainer formInput"
          displayExpr={labelKey || "name"}
          valueKey={valueKey || "value"}
        >
          <DropDownOptions showTitle={false} />
        </Lookup>

        {invalid && <p className="formInputError">{error}</p>}
      </FormGroup>
    </>
  );
};

export default SmartCombo;
