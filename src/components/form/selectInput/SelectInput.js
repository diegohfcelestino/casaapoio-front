import PropTypes from "prop-types";
import React from "react";

import "../Form.css";
import { FormGroup, ControlLabel, InputPicker, Whisper, Tooltip } from "rsuite";
import removerAcentosEspacos from "../../../services/removerAcentos";

const SelectInput = ({
  autoFocus,
  label,
  disabled,
  form,
  setForm,
  invalid,
  error,
  state,
  stateBlur,
  placeholder,
  options,
  value,
  labelKey,
  valueKey,
  width,
  margin,
}) => {
  return (
    <div style={{ margin: margin ?? 8 }}>
      <FormGroup
        style={{ width: width || 240 }}
        className="rs-form-control-wrapper"
      >
        <ControlLabel>{label}</ControlLabel>
        <Whisper
          open={error && invalid ? true : false}
          trigger="none"
          placement={"bottomStart"}
          speaker={<Tooltip className="tooltip">{error}</Tooltip>}
        >
          <InputPicker
            autoFocus={autoFocus}
            className="selectInputContainer formInput"
            name={removerAcentosEspacos(label)}
            disabled={disabled}
            value={value}
            data={options}
            labelKey={labelKey || "name"}
            valueKey={valueKey || "value"}
            maxHeight={150}
            onBlur={() =>
              setForm({
                ...form,
                [stateBlur]: true,
              })
            }
            onChange={(value) =>
              setForm({
                ...form,
                [state]: value,
              })
            }
            placeholder={placeholder}
            cleanable={false}
          />
        </Whisper>
      </FormGroup>
    </div>
  );
};

SelectInput.propTypes = {
  autoFocus: PropTypes.any,
  disabled: PropTypes.any,
  error: PropTypes.any,
  form: PropTypes.any,
  invalid: PropTypes.any,
  label: PropTypes.any,
  labelKey: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
    })
  ),
  placeholder: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
  stateBlur: PropTypes.any,
  value: PropTypes.any,
  valueKey: PropTypes.string,
};

export default SelectInput;
