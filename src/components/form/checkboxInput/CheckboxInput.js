import PropTypes from "prop-types";
import React from "react";

import "../Form.css";
import { FormGroup, ControlLabel } from "rsuite";
import { FiSquare, FiCheckSquare } from "react-icons/fi";
import removerAcentosEspacos from "../../../services/removerAcentos";

const CheckboxInput = ({ label, value, disabled, form, setForm, state, width }) => {
  return (
    <>
      <FormGroup className="formFormGroup" style={{ width: width || 120 }}>
        <ControlLabel>{label}</ControlLabel>
        <div
          disabled={disabled}
          className="formCheckbox formInputGroup"
          name={removerAcentosEspacos(label)}
          style={{ cursor: "pointer" }}
          onClick={() => {
            setForm({
              ...form,
              [state]: !value,
            });
          }}
        >
          <div>
            {value ? <FiCheckSquare size="20" /> : <FiSquare size="20" />}
          </div>
          <div className="formCheckboxText">{value ? "Sim" : "Não"}</div>
        </div>
      </FormGroup>
    </>
  );
};

CheckboxInput.propTypes = {
  disabled: PropTypes.any,
  form: PropTypes.any,
  label: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
  value: PropTypes.any,
  width: PropTypes.number,
};

export default CheckboxInput;
