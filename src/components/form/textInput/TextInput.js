import PropTypes from "prop-types";
import React, { memo } from "react";

import "../Form.css";
import { FormGroup, ControlLabel, Input } from "rsuite";
import removerAcentosEspacos from "../../../services/removerAcentos";

const TextInput = ({
  autoFocus,
  label,
  disabled,
  form,
  value,
  setForm,
  invalid,
  error,
  state,
  stateBlur,
  placeholder,
  maxlength,
  type,
  min,
  max,
  textarea,
  width,
}) => {
  return (
    <div style={{ margin: 8 }}>
      <FormGroup
        className="rs-form-control-wrapper"
        style={{ width: width || 240 }}
      >
        <ControlLabel>{label}</ControlLabel>
        <Input
          autoFocus={autoFocus}
          className="formInput"
          type={type}
          name={removerAcentosEspacos(label)}
          disabled={disabled}
          maxLength={maxlength}
          componentClass={textarea ? "textarea" : "input"}
          style={textarea ? { resize: "auto" } : { width: "100%" }}
          min={min}
          max={max}
          value={value}
          onBlur={() =>
            setForm({
              ...form,
              [stateBlur]: true,
            })
          }
          onChange={(text) =>
            setForm({
              ...form,
              [state]: text,
            })
          }
          placeholder={placeholder}
        />
        {invalid && <p className="formInputError">{error}</p>}
      </FormGroup>
    </div>
  );
};

TextInput.propTypes = {
  autoFocus: PropTypes.any,
  disabled: PropTypes.any,
  error: PropTypes.any,
  form: PropTypes.any,
  invalid: PropTypes.any,
  label: PropTypes.any,
  max: PropTypes.any,
  maxlength: PropTypes.any,
  min: PropTypes.any,
  placeholder: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
  stateBlur: PropTypes.any,
  textarea: PropTypes.any,
  type: PropTypes.any,
  value: PropTypes.any,
  type: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
  textarea: PropTypes.bool,
  width: PropTypes.number,
};

Input.propTypes = {
  disabled: PropTypes.any,
  error: PropTypes.any,
  form: PropTypes.any,
  invalid: PropTypes.any,
  label: PropTypes.any,
  onBlur: PropTypes.any,
  onChange: PropTypes.any,
  placeholder: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
};

export default memo(TextInput);
