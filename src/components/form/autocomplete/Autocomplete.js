import PropTypes from "prop-types";
import React from "react";

import "../Form.css";
import { FormGroup, ControlLabel, InputPicker, HelpBlock, Icon } from "rsuite";
import removerAcentosEspacos from "../../../services/removerAcentos";

const Autocomplete = ({
  autoFocus,
  label,
  disabled,
  form,
  setForm,
  invalid,
  error,
  state,
  stateBlur,
  placeholder,
  options,
  value,
  optionName,
  optionValue,
  onSearch,
  onSelect,
  onClean,
  loading,
  notCleanable,
  width,
}) => {
  return (
    <>
      <FormGroup className="formFormGroup" style={{ width: width || 250 }}>
        <ControlLabel>{label}</ControlLabel>
        <InputPicker
          autoFocus={autoFocus}
          className="selectInputContainer formInput"
          disabled={disabled}
          name={removerAcentosEspacos(label)}
          data={options}
          value={value}
          labelKey={optionName}
          valueKey={optionValue}
          onBlur={() => {
            if (form)
              setForm({
                ...form,
                [stateBlur]: true,
              });
          }}
          onChange={(value) => {
            if (form)
              setForm({
                ...form,
                [state]: value,
              });
          }}
          placeholder={placeholder}
          onSelect={(value, item) => onSelect(value, item)}
          onSearch={(value) => onSearch(value)}
          cleanable={!notCleanable}
          onClean={() => {
            if (!notCleanable) onClean();
          }}
          renderMenu={(menu) => {
            if (loading) {
              return (
                <p style={{ padding: 4, color: "#999", textAlign: "center" }}>
                  <Icon icon="spinner" spin /> Carregando...
                </p>
              );
            }
            return menu;
          }}
        />
        {invalid && <HelpBlock className="formInputError">{error}</HelpBlock>}
      </FormGroup>
    </>
  );
};

Autocomplete.propTypes = {
  autoFocus: PropTypes.any,
  disabled: PropTypes.any,
  error: PropTypes.any,
  form: PropTypes.any,
  invalid: PropTypes.any,
  label: PropTypes.any,
  loading: PropTypes.any,
  notCleanable: PropTypes.any,
  onClean: PropTypes.func,
  onSearch: PropTypes.func,
  onSelect: PropTypes.func,
  optionName: PropTypes.any,
  optionValue: PropTypes.any,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
    })
  ),
  placeholder: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.any,
  stateBlur: PropTypes.any,
  value: PropTypes.any,
  width: PropTypes.number,
};

export default Autocomplete;
