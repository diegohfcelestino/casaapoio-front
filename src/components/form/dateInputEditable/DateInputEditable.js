import PropTypes from "prop-types";
import React from "react";

import "../Form.css";
import { FormGroup, ControlLabel } from "rsuite";

const DateInputEditable = ({
  label,
  value,
  margin,
  setForm,
  invalid,
  error,
  state,
  disabled = false,
  onChange,
  maxDate,
  width,
}) => {
  return (
    <div style={{ margin: margin ?? 8 }}>
      <FormGroup className="rs-form-control-wrapper">
        <ControlLabel>{label}</ControlLabel>
        <input
          id="nascimento"
          className="rs-input"
          type="date"
          onChange={(e) => {
            if (e.target.value) {
              if (setForm) {
                setForm((form) => ({
                  ...form,
                  [state]: new Date(`${e.target.value}T03:00:00Z`),
                }));
              }
              if (onChange) {
                onChange({
                  iso: `${e.target.value}T03:00:00Z`,
                  date: new Date(`${e.target.value}T03:00:00Z`),
                });
              }
            }
          }}
          value={value ? new Date(value).toISOString().slice(0, 10) : ""}
          max={maxDate ? new Date(maxDate).toISOString().slice(0, 10) : null}
          style={{ width: width ?? "100%", maxHeight: 36 }}
          disabled={disabled}
        />
        {invalid && <p className="formInputError">{error}</p>}
      </FormGroup>
    </div>
  );
};

DateInputEditable.propTypes = {
  disabled: PropTypes.bool,
  error: PropTypes.string,
  invalid: PropTypes.any,
  label: PropTypes.string,
  margin: PropTypes.any,
  maxDate: PropTypes.any,
  setForm: PropTypes.func,
  state: PropTypes.string,
  width: PropTypes.string,
  value: PropTypes.any,
};

export default DateInputEditable;
