import Home from "../pages/home/Home";
import NotFound from "../pages/notFound/NotFound";
import LinkIndisponivel from "../pages/linkIndisponivel/LinkIndisponivel";
import Demanda from "../pages/demanda/Demanda";
import Leito from "../pages/leito/Leito";
import Ala from "../pages/ala/Ala";
import Quarto from "../pages/quarto/Quarto";
import SubLeito from "../pages/subLeito/SubLeito";
import ComposicaoAcomodacao from "../pages/composicaoAcomodacao/ComposicaoAcomodacao";
import Pessoa from "../pages/pessoa/Pessoa";

const Pages = [
  {
    name: "home",
    route: "/",
    component: Home,
  },
  {
    name: "linkIndisponivel",
    route: "/link-indisponivel",
    component: LinkIndisponivel,
  },
  {
    name: "demanda",
    route: "/demanda",
    component: Demanda,
  },
  {
    name: "leito",
    route: "/leito",
    component: Leito,
  },
  {
    name: "ala",
    route: "/ala",
    component: Ala,
  },
  {
    name: "quarto",
    route: "/quarto",
    component: Quarto,
  },
  {
    name: "subleito",
    route: "/subleito",
    component: SubLeito,
  },
  {
    name: "composicao-acomodacao",
    route: "/composicao-acomodacao",
    component: ComposicaoAcomodacao,
  },
  {
    name: "pessoa",
    route: "/pessoa",
    component: Pessoa,
  },
  {
    name: "pageNotFound",
    route: "*",
    component: NotFound,
  },
];

export default Pages;
