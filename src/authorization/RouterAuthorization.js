import React from "react";
import {userHasPermission, getInfo} from "../pages/login/LoginService"

const RouteAuthorization = (WrappedComponent, name) => {
  return class WithAuthorization extends React.Component {
    render() {
      let allowed = userHasPermission(name);
      return (
        <WrappedComponent
          {...this.props}
          auth={getInfo()}
          style={{ display: `${allowed ? "initial" : "none"}` }}
        />
      );
    }
  };
};

export default RouteAuthorization;
