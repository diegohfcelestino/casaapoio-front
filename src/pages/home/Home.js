import React, { useEffect, useState } from "react";

import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import * as FontAwesome from "react-icons/fa";
import "./Home.css";

const CardItem = ({ item }) => {
  const tabs = useSelector((state) => state.Header.tabs);
  const [tabsOpen, setTabsOpen] = useState([]);

  useEffect(() => {
    let storageTabs = localStorage.getItem("tabs", tabs);
    storageTabs = JSON.parse(storageTabs);
    setTabsOpen(storageTabs);
  }, [tabs]);

  useEffect(() => {
    tabs && setTabsOpen(tabs);
  }, [tabs]);

  function openTab() {
    let tabExists = false;
    let descricao = item.idRelatorio
      ? `Relatório - ` + item.descricao
      : item.descricao;
    tabsOpen.map((value) => {
      if (descricao === value.tab.descricao) {
        tabExists = true;
      }
      return item;
    });
    if (!tabExists) {
      tabsOpen.unshift({ tab: { ...item, descricao: descricao } });
    }

    localStorage.setItem("tabs", JSON.stringify([...tabsOpen]));
    localStorage.setItem("activeTab", JSON.stringify(descricao));
  }

  const Icon = FontAwesome[item.nomeImagem];
  return (
    <div>
      <Link
        title={`Ir para ${item.descricao}`}
        onClick={() => openTab()}
        to={{
          pathname: item.url,
          state: { idRelatorio: item.idRelatorio, nome: item.descricao },
          key: item.id,
        }}
        className="cardShortcut"
        id={item.id}
      >
        <div className="cardIcon">
          {item.nomeImagem ? (
            <Icon size="30" className="icon" />
          ) : (
            <FontAwesome.FaAlignLeft size="30" className="icon" />
          )}
        </div>
        <span>{item.descricao}</span>
      </Link>
    </div>
  );
};

const Home = () => {
  const menus = localStorage.getItem("menus")
    ? JSON.parse(localStorage.getItem("menus"))
    : [];
  return (
    <div className="homeContainer">
      <div className="shortcutContainer">
        {menus.length ? (
          <h3 className="title">Últimas páginas visualizadas</h3>
        ) : null}

        <div className="shortcutCards">
          {menus.map((item, i) => (
            <CardItem item={item} key={i} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
