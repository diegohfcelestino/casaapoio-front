import { useToasts } from "react-toast-notifications";
import { Form, FormGroup } from "rsuite";
import "../Page.css";
import AlaGrid from "./AlaGrid";

const Ala = () => {
  const { addToast } = useToasts();

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }

  return (
    <>
      <div className="pageContainer">
        <div className="pageFlex">
          <div className="pageContent withoutSearch w650">
            <h4>Ala</h4>
            <Form className="pageForm">
              <FormGroup
                className="pageFormContainer"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <AlaGrid
                  successMessage={successMessage}
                  warningMessage={warningMessage}
                />
              </FormGroup>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Ala;
