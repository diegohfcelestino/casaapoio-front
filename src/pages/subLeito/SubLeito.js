import { Form, FormGroup } from "rsuite";
import { useToasts } from "react-toast-notifications";
import "../Page.css";
import SubLeitoGrid from "./SubLeitoGrid";

const SubLeito = () => {
  const { addToast } = useToasts();

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }
  return (
    <>
      <div className="pageContainer">
        <div className="pageFlex">
          <div className="pageContent withoutSearch w650">
            <h4>Sub Leito</h4>
            <Form className="pageForm">
              <FormGroup
                className="pageFormContainer"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <SubLeitoGrid
                  successMessage={successMessage}
                  warningMessage={warningMessage}
                />
              </FormGroup>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default SubLeito;
