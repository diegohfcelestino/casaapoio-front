import React, { useState } from "react";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
} from "devextreme-react/data-grid";

import { gridDataSource } from "./DermandaServices"; /*copiado do projeto oficial*/

/*Mensagens vao aparecer quando executar determinada funcoes*/
const DemandaGrid = ({ successMessage, warningMessage }) => {
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];

  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  //Success
  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }
  //Error
  function handleInsertError(event) {
    const { newData } = event;
    const required = typeof newData.descricao !== "string";
    const length = !required ? newData.descricao.length > 50 : false;

    event.isValid = !required && !length;

    if (required) warningMessage("Informe a descrição");
    if (length) warningMessage("O limite de caracteres é de 50");
  }
  //Delete
  function handleRemove({ data }) {
    successMessage("Deletado com sucesso");
  }
  //Update
  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  return (
    <>
      <DataGrid
        dataSource={gridDataSource}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowValidating={handleInsertError}
        onOptionChanged={handleOptionChange}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />
        <Column
          dataField="descricao"
          caption="Descrição"
          filterOperations={false}
        >
          <HeaderFilter groupInterval={100} />
        </Column>

        <Column
          dataField="dataInclusao"
          caption="Data Inclusão"
          dataType="date"
          format="dd/MM/yyyy"
          alignment="center"
          width={150}
          allowEditing={false}
          filterOperations={false}
        >
          <HeaderFilter groupInterval={100} />
        </Column>
      </DataGrid>
    </>
  );
};

export default DemandaGrid;
