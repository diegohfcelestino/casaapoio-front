import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../services/api";
import { Auth } from "../../config/storage";
import { handleError, handleParams } from "../../services/helper";

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCaDemanda: 0,
    ...values,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("CaDemanda", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaDemanda/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCaDemanda }) {
  return api
    .delete(`CaDemanda/Id/${idCaDemanda}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...key,
    ...values,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .put("CaDemanda", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCaDemanda", "dataInclusao"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
