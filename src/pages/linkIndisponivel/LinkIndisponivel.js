import React from "react";
import image from "../../assets/images/linkUnavailable.svg";

import "./LinkIndisponivel.css";

const LinkIndisponivel = () => {
  return (
    <div className="linkIndisponivelContainer">
      <h2>Link Indisponível</h2>
      <img src={image} alt="link indisponível" width={300} />
    </div>
  );
};

export default LinkIndisponivel;
