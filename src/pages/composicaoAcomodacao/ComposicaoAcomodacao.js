import { useState } from "react";
import { useDispatch } from "react-redux";
import { Form, FormGroup, FlexboxGrid } from "rsuite";
import { useToasts } from "react-toast-notifications";
import "../Page.css";
import SelectDataPicker from "../../components/selectDataPicker/SelectDataPicker";
import {
  addIdItem,
  gridDataSourceAla,
} from "../../store/modules/composicaoAcomodacao/action";

import ComposicaoAcomodacaoGrid from "./ComposicaoAcomodacaoGrid";

const ComposicaoAcomodacao = () => {
  const { addToast } = useToasts();
  const dispatch = useDispatch();
  const [form, setForm] = useState({ idCaAla: 0, ala: "" });
  const columns = [{ dataField: "ala", caption: "Ala" }];

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function errorMessage(message) {
    addToast(message, {
      appearance: "error",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }

  return (
    <div className="pageContainer">
      <div className="pageFlex">
        <div
          className="pageContent withoutSearch"
          style={{ width: 1200, maxWidth: "100%" }}
        >
          <h4>Composição Acomodação</h4>
          <Form className="pageForm" fluid>
            <FlexboxGrid.Item colspan={24} style={{ margin: "8px 0 16px" }}>
              <SelectDataPicker
                dataSource={gridDataSourceAla}
                label="Ala"
                displayExpr="ala"
                valueExpr="idCaAla"
                value={form.idCaAla}
                editValue={form.ala}
                colums={columns}
                onChange={({ idCaAla }) => {
                  if (idCaAla) {
                    setForm({ idCaAla });
                    dispatch(addIdItem(idCaAla));
                  }
                }}
                placeholder="Selecione a Ala"
                style={{ width: "800px", maxWidth: "100%" }}
              />
            </FlexboxGrid.Item>
            {form.idCaAla !== 0 && (
              <FormGroup className="pageFormContainer">
                <ComposicaoAcomodacaoGrid
                  form={form}
                  successMessage={successMessage}
                  errorMessage={errorMessage}
                  warningMessage={warningMessage}
                />
              </FormGroup>
            )}
          </Form>
        </div>
      </div>
    </div>
  );
};

export default ComposicaoAcomodacao;
