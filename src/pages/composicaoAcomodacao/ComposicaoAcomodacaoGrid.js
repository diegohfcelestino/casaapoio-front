import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Lookup,
  Pager,
  Paging,
  RequiredRule,
} from "devextreme-react/data-grid";

import {
  gridDataSource,
  gridDataSourceBed,
  gridDataSourceSubGrade,
  gridDataSourceRoom,
  selectItem,
} from "../../store/modules/composicaoAcomodacao/action";

const ComposicaoAcomodacaoGrid = ({ form, successMessage }) => {
  const dispatch = useDispatch();
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);
  let dataGrid = {};

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  useEffect(() => {
    if (dataGrid) {
      dataGrid.instance.refresh();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [form.idCaAla]);

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleRemove({ data }) {
    successMessage("Composição acomodação deletada com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  function handleUpdating({ oldData, newData }) {
    dispatch(
      selectItem({
        ...oldData,
        ...newData,
      })
    );
  }

  function handleCellRender({ data }) {
    return <p>{data?.quarto}</p>;
  }

  return (
    <>
      <DataGrid
        width="100%"
        ref={(ref) => (dataGrid = ref)}
        dataSource={gridDataSource}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowUpdating={handleUpdating}
        onOptionChanged={handleOptionChange}
      >
        <Pager
          allowedPageSizes={[50, 75, 150]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idCaQuarto"
          caption="Quarto"
          filterOperations={false}
          cellRender={handleCellRender}
        >
          <Lookup
            dataSource={gridDataSourceRoom}
            displayExpr="quarto"
            valueExpr="idCaQuarto"
          />
          <RequiredRule />
        </Column>

        <Column dataField="idCaLeito" caption="Leito" filterOperations={false}>
          <Lookup
            dataSource={gridDataSourceBed}
            displayExpr="leito"
            valueExpr="idCaLeito"
          />
        </Column>

        <Column
          dataField="idCaSubLeito"
          caption="Sub Leito"
          filterOperations={false}
        >
          <Lookup
            dataSource={gridDataSourceSubGrade}
            displayExpr="subLeito"
            valueExpr="idCaSubLeito"
          />
        </Column>

        <Column
          dataField="dataInclusao"
          caption="Data Inclusão"
          dataType="date"
          format="dd/MM/yyyy"
          alignment="center"
          allowEditing={false}
          filterOperations={false}
          width={150}
        >
          <HeaderFilter groupInterval={100} />
        </Column>
      </DataGrid>
    </>
  );
};

export default ComposicaoAcomodacaoGrid;
