import { Form, FormGroup } from "rsuite";
import { useToasts } from "react-toast-notifications";

import "../Page.css";

import QuartoGrid from "./QuartoGrid";

const Quarto = () => {
  const { addToast } = useToasts();

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }

  return (
    <div className="pageContainer">
      <div className="pageFlex">
        <div className="pageContent withoutSearch w650">
          <h4>Quarto</h4>
          <Form className="pageForm">
            <FormGroup
              className="pageFormContainer"
              style={{ display: "flex", justifyContent: "center" }}
            >
              <QuartoGrid
                successMessage={successMessage}
                warningMessage={warningMessage}
              />
            </FormGroup>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Quarto;
