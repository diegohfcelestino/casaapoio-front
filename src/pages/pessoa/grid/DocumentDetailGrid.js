import React, { useEffect, useState, useMemo, useCallback } from "react";
import { useDispatch } from "react-redux";
import {
  handleGetPorPessoaDocumentoImg,
  handleDeleteDocumentoImg,
} from "../PessoaServices";
import { Modal, Button, Loader } from "rsuite";
import Document from "../Document";
import { FiPlusCircle, FiTrash2 } from "react-icons/fi";
import { FaFilePdf } from "react-icons/fa";
import { useToasts } from "react-toast-notifications";

import { handleErrorForm } from "../../../services/helper";

function DocumentDetailGrid({ data }) {
  const [list, setList] = useState([]);
  const [showFile, setShowFile] = useState({});
  const [modalOpen, setModalOpen] = useState(false);
  const [modalImgOpen, setModalImgOpen] = useState(false);
  const [modalDocOpen, setModalDocOpen] = useState(false);
  const [modalRemoveBtn, setModalRemoveBtn] = useState(false);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const { addToast } = useToasts();

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }

  function errorMessage(message) {
    addToast(message, {
      appearance: "error",
      autoDismiss: true,
    });
  }

  function getFileList() {
    setLoading(true);

    handleGetPorPessoaDocumentoImg(data.key)
      .then((res) => {
        if (res.data) setList([...res.data]);
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao carregar", err));
      })
      .finally(() => setLoading(false));
  }

  useEffect(() => {
    getFileList();
  }, []);

  const renderItems = useMemo(
    () =>
      list.map((file, index) => {
        return (
          <div
            key={index}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              width: 100,
            }}
          >
            {file.extensao === ".pdf" ? (
              <div
                className="previewFile"
                onClick={() => {
                  setShowFile({
                    arquivoBase64: file.arquivoBase64,
                    extensao: file.extensao,
                    nomeArquivo: file.nomeArquivo,
                  });
                  setModalDocOpen(true);
                }}
              >
                <FaFilePdf size={100} />
              </div>
            ) : (
              <div
                className="previewFile"
                onClick={() => {
                  setShowFile({
                    arquivoBase64: file.arquivoBase64,
                    extensao: file.extensao,
                    nomeArquivo: file.nomeArquivo,
                  });
                  setModalImgOpen(true);
                }}
              >
                <img
                  id="preview"
                  src={
                    file.arquivoBase64 && file.extensao
                      ? `data:${file.extensao};base64,${file.arquivoBase64}`
                      : ""
                  }
                  alt={file.nomeArquivo ?? ""}
                  style={{ height: 100, width: 100 }}
                />
              </div>
            )}
            <div
              className="previewLabel ellipsis"
              onClick={() => {
                let removeFile = { ...file };
                setShowFile(removeFile);
                setModalRemoveBtn(true);
              }}
            >
              <FiTrash2 color="orange" size={18} />
              <span style={{ maxWidth: 100 }}>{file.nomeArquivo ?? ""}</span>
            </div>
          </div>
        );
      }),
    [list]
  );

  return (
    <>
      {showFile && (
        <>
          <Modal show={modalRemoveBtn} onHide={() => setModalRemoveBtn(false)}>
            <Modal.Header>
              <Modal.Title>{`Deseja realmente excluir o arquivo (${showFile.nomeArquivo})?`}</Modal.Title>
              <Modal.Body
                style={{ width: "100%", justifyContent: "space-between" }}
              >
                <Button onClick={() => setModalRemoveBtn(false)}>
                  Cancelar
                </Button>
                <Button
                  color="orange"
                  onClick={() =>
                    handleDeleteDocumentoImg(showFile.idCaPessoaDocumenImagem)
                      .then((res) => {
                        successMessage(res);
                        setList([]);
                        getFileList();
                      })
                      .catch((err) => errorMessage(err))
                      .finally(() => setModalRemoveBtn(false))
                  }
                >
                  Excluir Permanente
                </Button>
              </Modal.Body>
            </Modal.Header>
          </Modal>

          <Modal full show={modalImgOpen} onHide={() => setModalImgOpen(false)}>
            <Modal.Header>
              <Modal.Title>{`Vizualizar${showFile.nomeArquivo}`}</Modal.Title>
              <Modal.Body>
                <img
                  style={{ width: "100%", height: "80vh" }}
                  src={`data:${showFile.extensao};base64,${showFile.arquivoBase64}`}
                  alt={showFile.nomeArquivo}
                />
              </Modal.Body>
            </Modal.Header>
          </Modal>

          <Modal full show={modalDocOpen} onHide={() => setModalDocOpen(false)}>
            <Modal.Header>
              <Modal.Title>Vizualizar Danfe</Modal.Title>
              <Modal.Body>
                <embed
                  style={{ width: "100%", height: "80vh" }}
                  type="application/pdf"
                  src={`data:application/pdf;base64,${showFile.arquivoBase64}`}
                />
              </Modal.Body>
            </Modal.Header>
          </Modal>
        </>
      )}
      <Document
        idCaPessoaDocumenImagem={data.key}
        count={list.length}
        getFileList={() => getFileList()}
        modalOpen={modalOpen}
        handleModal={setModalOpen}
        warningMessage={warningMessage}
        successMessage={successMessage}
        errorMessage={errorMessage}
      />
      <h4>Arquivos</h4>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          maxWidth: "100%",
          flexWrap: "wrap",
        }}
      >
        <div
          className="docPicker"
          onClick={() =>
            list.length < 5
              ? setModalOpen(!modalOpen)
              : warningMessage("Número máximo de arquivos atingido")
          }
        >
          <FiPlusCircle size={50} />
        </div>

        {!loading ? (
          <>
            {list.length ? renderItems : <span>Nenhum arquivo encontrado</span>}
          </>
        ) : (
          <Loader />
        )}
      </div>
    </>
  );
}

export default DocumentDetailGrid;
