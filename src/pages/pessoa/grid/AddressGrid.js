import React, { useState } from "react";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
} from "devextreme-react/data-grid";
import axios from "axios";
import Cleave from "cleave.js/react";
import { Checkbox } from "rsuite";
import { useToasts } from "react-toast-notifications";
import {
  gridDataSourceAddress,
  gridDataSourceAddressType,
  gridDataSourceCidade,
  gridDataSourceUf,
  gridDataSourceTipoLogradouro,
  handleCepInfo,
} from "../../../store/modules/person/action";
import { handleUpdateEndereco } from "../PessoaServices";
import ModalSelect from "../../../components/modalSelect/ModalSelect";
import { handleErrorForm } from "../../../services/helper";
import removerAcentosEspacos from "../../../services/removerAcentos";

const AddressGrid = ({ idCaPessoa, successMessage, errorMessage }) => {
  const { addToast } = useToasts();

  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);
  const [toggleCidade, setToggleCidade] = useState(false);
  const [toggleEstado, setToggleEstado] = useState(false);
  const [toggleLogradouro, setToggleLogradouro] = useState(false);
  const [listCidade, setListCidade] = useState([]);
  const [listEstado, setListEstado] = useState([]);
  const [listLogradouro, setListLogradouro] = useState([]);
  const [idCidade, setIdCidade] = useState(0);
  const [idUF, setIdUF] = useState(0);
  const [idLogradouro, setIdLogradouro] = useState(0);
  const [key, setKey] = useState([]);
  const [listIds, setListIds] = useState([]);
  const [sigla, setSigla] = useState("");
  const [endereco, setEndereco] = useState("");
  const [bairro, setBairro] = useState("");
  const [descricaoCidade, setDescricaoCidade] = useState("");
  const [descricaoEstado, setDescricaoEstado] = useState("");
  const [allowAdding, setAllowAdding] = useState(true);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    for (let i = 0; i < 9; i++) {
      if (event.fullName === `columns[${i}].filterValue`) {
        setAllowAdding(true);
      }
    }

    if (event.fullName === "editing.changes") {
      if (event.value[0]?.data) {
        event.value[0].data.descricaoCidade = descricaoCidade;
        event.value[0].data.endereco = endereco;
        event.value[0].data.bairro = bairro;
      }
    }
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleRemove({ data }) {
    successMessage("Endereço deletado com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  function editCellCidade(event) {
    const { data } = event.data;

    listCidade.map((item) => {
      const checkIfHasId = item.idCaPessoaEndereco !== undefined;
      const checkIds =
        item.idCaPessoaEndereco === data.idCaPessoaEndereco ||
        item.idCaPessoaEndereco === data.id;

      if (checkIfHasId && checkIds) {
        data.idCaPessoaEndereco =
          item.idCaPessoaEndereco ?? data.idCaPessoaEndereco;
        data.descricaoCidade = item.descricaoCidade ?? descricaoCidade;
        data.value = descricaoCidade;
        event.data.setValue(descricaoCidade, "descricaoCidade");
      }

      return item;
    });

    return handleInputModalCidade(event.data);
  }

  function editCellEstado(event) {
    const { data } = event.data;

    listEstado.map((item) => {
      const checkIfHasId = item.idCaPessoaEndereco !== undefined;
      const checkIds =
        item.idCaPessoaEndereco === data.idCaPessoaEndereco ||
        item.idCaPessoaEndereco === data.id;

      if (checkIfHasId && checkIds) {
        data.idCaPessoaEndereco =
          item.idCaPessoaEndereco ?? data.idCaPessoaEndereco;
        data.descricaoUf = item.descricaoEstado ?? descricaoEstado;

        data.value = descricaoEstado;

        event.data.setValue(descricaoEstado, "descricaoUf");
      }

      return item;
    });

    return handleInputModalEstado(event.data);
  }

  function editCellLogradouro(event) {
    const { data } = event.data;

    listLogradouro.map((item) => {
      const checkIfHasId = item.idCaPessoaEndereco !== undefined;
      const checkIds =
        item.idCaPessoaEndereco === data.idCaPessoaEndereco ||
        item.idCaPessoaEndereco === data.id;

      if (checkIfHasId && checkIds) {
        data.idCaPessoaEndereco =
          item.idCaPessoaEndereco ?? data.idCaPessoaEndereco;
        data.descricaoLogradouro =
          item.descricaoLogradouro ?? data.descricaoLogradouro;
        data.value = item.descricaoLogradouro;

        event.data.setValue(item.descricaoLogradouro, "descricaoLogradouro");
      }

      return item;
    });

    return handleInputModalLogradouro(event.data);
  }

  function toggleModalCidade(data) {
    setToggleCidade(!toggleCidade);

    if (!data) return;
    if (
      data.idCaPessoaEndereco !== undefined ||
      data.idCaPessoaEndereco !== null
    ) {
      setKey(data.idCaPessoaEndereco);
    }

    // Pega item selecionado na modal
    if (data?.idCrCidades) {
      listCidade.map((item) => {
        if (item.idCaPessoaEndereco === key) {
          item.idCidade = data.idCrCidades;
          item.descricaoCidade = data.cidade;
          setDescricaoCidade(data.cidade);
          setIdCidade(data.idCrCidades);
          setListCidade([...listCidade]);
        }

        return item;
      });

      setListCidade([...listCidade]);
    }
  }

  function toggleModalEstado(data) {
    setToggleEstado(!toggleEstado);

    if (!data) return;
    if (
      data.idCaPessoaEndereco !== undefined ||
      data.idCaPessoaEndereco !== null
    ) {
      setKey(data.idCaPessoaEndereco);
    }

    // Pega item selecionado na modal
    if (data?.idCrUf) {
      listEstado.map((item) => {
        if (item.idCaPessoaEndereco === key) {
          item.idEstado = data.idCrUf;
          item.descricaoEstado = data.descricaouf;
          setIdUF(data.idCrUf);
          setSigla(data.siglauf);
          setDescricaoEstado(data.descricaouf);
          setListEstado([...listEstado]);
        }

        return item;
      });

      setListEstado([...listEstado]);
    }
  }

  function toggleModalLogradouro(data) {
    setToggleLogradouro(!toggleLogradouro);

    if (!data) return;
    if (
      data.idCaPessoaEndereco !== undefined ||
      data.idCaPessoaEndereco !== null
    ) {
      setKey(data.idCaPessoaEndereco);
    }

    // Pega item selecionado na modal
    if (data?.idFpTipoLogradouro) {
      listLogradouro.map((item) => {
        if (item.idCaPessoaEndereco === key) {
          item.idLogradouro = data.idFpTipoLogradouro;
          item.descricaoLogradouro = data.descricao;
          setIdLogradouro(data.idFpTipoLogradouro);
          setListLogradouro([...listLogradouro]);
        }

        return item;
      });

      setListLogradouro([...listLogradouro]);
    }
  }

  function handleInputModalCidade(event) {
    return (
      <div
        className="ellipsis"
        style={{
          cursor: "pointer",
          height: 22,
          display: "flex",
        }}
      >
        <div
          onClick={() => {
            toggleModalCidade({
              idCaPessoaEndereco: event.data.idCaPessoaEndereco || 0,
            });
          }}
          style={{ width: "100%", paddingLeft: "7px" }}
        >
          {descricaoCidade ? descricaoCidade : ""}
        </div>
      </div>
    );
  }

  function handleInputModalEstado(event) {
    return (
      <div
        className="ellipsis"
        style={{
          cursor: "pointer",
          height: 22,
          display: "flex",
        }}
      >
        <div
          onClick={() => {
            toggleModalEstado({
              idCaPessoaEndereco: event.data.idCaPessoaEndereco || 0,
            });
          }}
          style={{ width: "100%", paddingLeft: "7px" }}
        >
          {descricaoEstado ? descricaoEstado : ""}
        </div>
      </div>
    );
  }

  function handleInputModalLogradouro(event) {
    return (
      <div
        className="ellipsis"
        style={{
          cursor: "pointer",
          height: 22,
          display: "flex",
        }}
      >
        <div
          onClick={() => {
            toggleModalLogradouro({
              idCaPessoaEndereco: event.data.idCaPessoaEndereco || 0,
            });
          }}
          style={{ width: "100%", paddingLeft: "7px" }}
        >
          {event.value ?? ""}
        </div>
      </div>
    );
  }

  function handleNewRow(event) {
    event.data.idCaPessoaEndereco = 0;
    listCidade.push({ idCaPessoaEndereco: 0 });
    setListCidade([...listCidade]);

    listEstado.push({ idCaPessoaEndereco: 0 });
    setListEstado([...listEstado]);

    listLogradouro.push({ idCaPessoaEndereco: 0 });
    setListLogradouro([...listLogradouro]);
  }

  function handleEditingStart(event) {
    setAllowAdding(false);

    listCidade.push({
      idCaPessoaEndereco: event.data.idCaPessoaEndereco,
      descricaoCidade: event.data.descricaoCidade,
      idCidade: event.data.idCidade,
    });
    setDescricaoCidade(event.data.descricaoCidade);
    setIdCidade(event.data.idCidade);
    setListCidade([...listCidade]);

    listEstado.push({
      idCaPessoaEndereco: event.data.idCaPessoaEndereco,
      descricaoEstado: event.data.descricaoUf,
      idEstado: event.data.idUF,
    });
    setIdUF(event.data.idUF);
    setDescricaoEstado(event.data.descricaoUf);

    setListEstado([...listEstado]);

    listLogradouro.push({
      idCaPessoaEndereco: event.data.idCaPessoaEndereco,
      descricaoLogradouro: event.data.descricaoLogradouro,
      idLogradouro: event.data.idLogradouro,
    });
    setIdLogradouro(event.data.idLogradouro);
    setListLogradouro([...listLogradouro]);

    setEndereco(event.data.endereco);
    setBairro(event.data.bairro);
  }

  async function handleInsertError(event) {
    const { newData, oldData = false } = event;

    newData.idCidade = idCidade ?? oldData.idCidade;
    newData.idUF = idUF ?? oldData.idUF;
    newData.idLogradouro = idLogradouro ?? oldData.idLogradouro;
    newData.descricaoCidade = descricaoCidade ?? oldData.descricaoCidade;
    newData.descricaoEstado = descricaoEstado ?? oldData.descricaoEstado;
    newData.cep = newData?.cep ?? oldData?.cep;
    newData.numero = newData?.numero ?? oldData?.numero;
    newData.complemento = newData?.complemento ?? oldData?.complemento;
    newData.bairro = bairro ?? oldData?.bairro;
    newData.endereco = endereco ?? oldData?.endereco;
    newData.idCaTipoEndereco =
      newData.idCaTipoEndereco ?? oldData.idCaTipoEndereco;
    newData.dataInclusao = oldData.dataInclusao;
    newData.principal = oldData.principal ?? "F";
    newData.usuario = oldData.usuario ?? "";
  }

  const handleSelectionChanged = (event) => {
    let checked = event.data.principal === "T";
    let message = `Endereço   ${
      !checked ? "marcado" : "desmarcado"
    } como principal`;
    let id = 0;
    listIds.map((item) => {
      if (
        event.data.idCaPessoaEndereco === item.idCaPessoaEndereco &&
        checked
      ) {
        id = item.id;
      }
      return item;
    });

    let params = {
      ...event.data,
      idCaPessoaEndereco: id ? id : event.data.idCaPessoaEndereco,
      idCaTipoEndereco: event.data.idCaTipoEndereco,
      idCaPessoa: idCaPessoa,
      principal: checked ? "F" : "T",
      dataInclusao: event.data.dataInclusao,
    };
    handleUpdateEndereco(params)
      .then((res) => {
        event.data.principal = checked ? "T" : "F";
        if (event.data.principal) {
          listIds.push({
            id: res.idCaPessoaEndereco,
          });
          setListIds(listIds);
        }

        addToast(message, {
          appearance: "success",
          autoDismiss: true,
          transitionState: "entered",
        });
      })
      .catch((error) => {
        addToast(
          handleErrorForm(
            `Algo de errado aconteceu, não foi possível ${
              checked ? "desmarcar" : "marcar"
            }`,
            error
          ),
          {
            appearance: "error",
            autoDismiss: true,
            transitionState: "entered",
          }
        );
      });
  };

  const checkRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          checked={e.data.principal === "T" ? true : false}
          onCheckboxClick={() => handleSelectionChanged(e)}
        />
      </div>
    );
  };

  const editCheckRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          disabled={true}
          checked={e.data.principal === "T" ? true : false}
        />
      </div>
    );
  };

  async function handleCep(rowData, e) {
    const location = await axios
      .get(`https://viacep.com.br/ws/${e.target.rawValue}/json`)
      .then((res) => res.data);
    if (location.cep) {
      const cepInfo = await handleCepInfo(location);
      if (cepInfo.data[0]) {
        setIdCidade(cepInfo.data[0]?.idCrCidades);
        setIdUF(cepInfo.data[0]?.idUf);
        setSigla(cepInfo.data[0]?.uf);
        setDescricaoCidade(removerAcentosEspacos(cepInfo.data[0]?.cidade));
        setDescricaoEstado(
          removerAcentosEspacos(cepInfo.data[0]?.descricaoEstado)
        );
      }
      setEndereco(removerAcentosEspacos(location.logradouro));
      setBairro(removerAcentosEspacos(location.bairro));
    } else {
      setIdCidade(0);
      setIdUF(0);
      setSigla("");
      setDescricaoCidade("");
      setDescricaoEstado("");
      setEndereco("");
      setBairro("");
    }
  }

  function clearDadosGrid() {
    setAllowAdding(true);
    setIdCidade(0);
    setIdLogradouro(0);
    setIdUF(0);
    setSigla("");
    setEndereco("");
    setBairro("");
    setDescricaoCidade("");
    setDescricaoEstado("");
  }

  return (
    <>
      <ModalSelect
        dataSource={gridDataSourceCidade(sigla)}
        colums={[
          {
            dataField: "cidade",
            caption: "Cidade",
            width: "70%",
          },
          {
            dataField: "uf",
            caption: "UF",
            width: "10%",
          },
          {
            dataField: "codigo",
            caption: "Código IBGE",
            alignment: "right",
          },
        ]}
        title={"a Cidade"}
        visible={toggleCidade}
        toggleModal={toggleModalCidade}
        width="1200px"
      />
      <ModalSelect
        dataSource={gridDataSourceUf}
        colums={[
          {
            caption: "Estado",
            dataField: "descricaouf",
          },
        ]}
        title={"o Estado"}
        visible={toggleEstado}
        toggleModal={toggleModalEstado}
        width="1200px"
      />
      <ModalSelect
        dataSource={gridDataSourceTipoLogradouro}
        colums={[
          {
            caption: "Logradouro",
            dataField: "descricao",
          },
        ]}
        title={"o Logradouro"}
        visible={toggleLogradouro}
        toggleModal={toggleModalLogradouro}
        width="1200px"
      />
      <DataGrid
        dataSource={gridDataSourceAddress(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onInitNewRow={handleNewRow}
        onEditingStart={handleEditingStart}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowValidating={handleInsertError}
        onSaved={() => clearDadosGrid()}
        onEditCanceled={() => clearDadosGrid()}
        onRowUpdated={handleUpdate}
        onOptionChanged={handleOptionChange}
        loadPanel={false}
        style={{ marginTop: 20 }}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing
          mode="row"
          allowAdding={allowAdding}
          allowDeleting
          allowUpdating
          useIcons
        />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idCaTipoEndereco"
          caption="Tipo Endereço"
          filterOperations={false}
          width={"10%"}
        >
          <Lookup
            dataSource={gridDataSourceAddressType}
            displayExpr="descricao"
            valueExpr="idCaTipoEndereco"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="cep"
          caption="CEP"
          filterOperations={false}
          width={"7%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask "
              options={{
                blocks: [5, 3],
                delimiters: ["-", ""],
                numericOnly: true,
              }}
              onChange={(e) => {
                rowData.data.setValue(e.target.value, "cep");
                if (rowData.data.value.length === 0) {
                  setIdCidade(0);
                  setIdLogradouro(0);
                  setIdUF(0);
                  setSigla("");
                  setEndereco("");
                  setBairro("");
                  setDescricaoCidade("");
                  setDescricaoEstado("");
                }
                if (rowData.data.value.length === 9) {
                  handleCep(rowData, e);
                }
              }}
              value={rowData.data.value}
            />
          )}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="endereco"
          caption="Endereço"
          filterOperations={false}
          width={"13%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask"
              options={{ blocks: [99999], delimiter: "" }}
              onBlur={(e) => {
                setEndereco(e.target.value);
              }}
              onChange={(e) => {
                rowData.data.setValue(e.target.rawValue, "endereco");
              }}
              value={endereco}
            />
          )}
        >
          <RequiredRule />
        </Column>
        <Column
          dataField="bairro"
          caption="Bairro"
          filterOperations={false}
          width={"12%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask"
              options={{ blocks: [99999], delimiter: "" }}
              onBlur={(e) => {
                setBairro(e.target.value);
              }}
              onChange={(e) => {
                rowData.data.setValue(e.target.rawValue, "bairro");
              }}
              value={bairro}
            />
          )}
        >
          <RequiredRule />
        </Column>
        <Column
          width={"6%"}
          dataField="numero"
          caption="Número"
          filterOperations={false}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="complemento"
          caption="Complemento"
          filterOperations={false}
          width={"10%"}
        ></Column>

        <Column
          dataField="descricaoUf"
          caption="Estado"
          filterOperations={false}
          width={"13%"}
          editCellComponent={editCellEstado}
          alignment={"left"}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="descricaoCidade"
          caption="Cidade"
          filterOperations={false}
          width={"15%"}
          editCellComponent={editCellCidade}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="descricaoLogradouro"
          caption="Logradouro"
          filterOperations={false}
          width={"8%"}
          editCellComponent={editCellLogradouro}
          alignment={"left"}
        >
          <RequiredRule />
        </Column>

        <Column
          alignment="center"
          dataField="principal"
          caption={"Principal"}
          cellRender={checkRender}
          editCellRender={editCheckRender}
          width={"6%"}
          filterOperations={false}
        ></Column>
      </DataGrid>
    </>
  );
};

export default AddressGrid;
