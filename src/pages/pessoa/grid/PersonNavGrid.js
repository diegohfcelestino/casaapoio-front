import React, { useState, useMemo } from "react";
import { Nav } from "rsuite";

import "../../Page.css";

import AddressGrid from "./AddressGrid";
import PersonClassification from "./PersonClassification";
import ContactGrid from "./ContactGrid";
import DocumentGrid from "./DocumentGrid";
import EmailGrid from "./EmailGrid";
import FamilyCompositionGrid from "./FamilyCompositionGrid";
import PhoneGrid from "./PhoneGrid";
import SpecialNeedGrid from "./SpecialNeedGrid";
import PrecautionNeedGrid from "./PrecautionNeedGrid";

const PersonNavGrid = ({
  editing,
  personFisic,
  successMessage,
  errorMessage,
}) => {
  const [activeTab, setActiveTab] = useState("personClassification");

  const CustomNav = ({ active, onSelect, ...props }) => {
    return (
      <Nav {...props} activeKey={active} onSelect={onSelect}>
        <Nav.Item disabled={!editing} eventKey="personClassification">
          Classificação Pessoa
        </Nav.Item>
        <Nav.Item disabled={!editing} eventKey="contactGrid">
          Contato
        </Nav.Item>
        <Nav.Item disabled={!editing} eventKey="documentGrid">
          Documento
        </Nav.Item>
        <Nav.Item disabled={!editing} eventKey="emailGrid">
          Email
        </Nav.Item>
        <Nav.Item disabled={!editing} eventKey="addressGrid">
          Endereço
        </Nav.Item>
        <Nav.Item disabled={!editing} eventKey="phoneGrid">
          Telefone
        </Nav.Item>
        {personFisic && (
          <Nav.Item disabled={!editing} eventKey="specialNeedGrid">
            Necessidade Especial
          </Nav.Item>
        )}
        {personFisic && (
          <Nav.Item disabled={!editing} eventKey="precautionNeedGrid">
            Necessidade Precaução
          </Nav.Item>
        )}
        {personFisic && (
          <Nav.Item disabled={!editing} eventKey="familyCompositionGrid">
            Composição Familiar
          </Nav.Item>
        )}
      </Nav>
    );
  };

  const customGrid = useMemo(() => {
    const tabs = {
      personClassification: PersonClassification,
      addressGrid: AddressGrid,
      contactGrid: ContactGrid,
      documentGrid: DocumentGrid,
      familyCompositionGrid: FamilyCompositionGrid,
      emailGrid: EmailGrid,
      phoneGrid: PhoneGrid,
      specialNeedGrid: SpecialNeedGrid,
      precautionNeedGrid: PrecautionNeedGrid,
    };

    const CustomComponent = editing ? tabs[activeTab] : "";
    return CustomComponent ? (
      <CustomComponent
        idCaPessoa={editing}
        successMessage={successMessage}
        errorMessage={errorMessage}
      />
    ) : null;
  }, [activeTab, editing, errorMessage, successMessage]);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <CustomNav
        appearance="tabs"
        active={editing ? activeTab : ""}
        onSelect={(newTab) => setActiveTab(newTab)}
      />
      {customGrid}
    </div>
  );
};

export default PersonNavGrid;
