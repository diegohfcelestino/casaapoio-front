import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
} from "devextreme-react/data-grid";

import {
  gridDataSourceContact,
  gridDataSourceContactType,
} from "../../../store/modules/person/action";

const ContactGrid = ({ idCaPessoa, successMessage, errorMessage }) => {
  const dispatch = useDispatch();
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleInsertError(event) {
    const { newData, oldData = false } = event;

    newData.idCaTipoContato =
      newData.idCaTipoContato ?? oldData.idCaTipoContato;
    newData.contato = newData.contato ?? oldData.contato;
    newData.dataInclusao = oldData.dataInclusao;
    newData.usuario = oldData.usuario;
  }

  function handleRemove({ data }) {
    successMessage("Contato deletado com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourceContact(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowValidating={handleInsertError}
        onOptionChanged={handleOptionChange}
        style={{ marginTop: 20, width: "65%" }}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idCaTipoContato"
          caption="Tipo Contato"
          filterOperations={false}
          width={"35%"}
        >
          <Lookup
            dataSource={gridDataSourceContactType}
            displayExpr="descricao"
            valueExpr="idCaTipoContato"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="contato"
          caption="Contato"
          filterOperations={false}
          width={"50%"}
        >
          <RequiredRule />
        </Column>
      </DataGrid>
    </div>
  );
};

export default ContactGrid;
