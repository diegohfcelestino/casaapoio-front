import React, { useState } from "react";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
  EmailRule,
} from "devextreme-react/data-grid";
import { handleUpdateEmail } from "../PessoaServices";
import { Checkbox } from "rsuite";
import { useToasts } from "react-toast-notifications";
import {
  gridDataSourceEmail,
  gridDataSourceEmailType,
} from "../../../store/modules/person/action";
import { handleErrorForm } from "../../../services/helper";

const EmailGrid = ({ idCaPessoa, successMessage, errorMessage }) => {
  const { addToast } = useToasts();

  const [listIds, setListIds] = useState([]);
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleInsertError(event) {
    const { newData, oldData = false } = event;

    newData.email = newData.email ?? oldData.email;
    newData.idCaTipoEmail = newData.idCaTipoEmail ?? oldData.idCaTipoEmail;
    newData.principal = oldData.principal ?? "F";
    newData.dataInclusao = oldData.dataInclusao;
    newData.usuario = oldData.usuario;
  }

  function handleRemove({ data }) {
    successMessage("Email deletado com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  const handleSelectionChanged = (event) => {
    let checked = event.data.principal === "T";
    let message = `Email   ${
      !checked ? "marcado" : "desmarcado"
    } como principal`;
    let id = 0;
    listIds.map((item) => {
      if (event.data.idCaPessoaEmail === item.idCaPessoaEmail && checked) {
        id = item.id;
      }
      return item;
    });

    let params = {
      idCaPessoaEmail: id ? id : event.data.idCaPessoaEmail,
      idCaTipoEmail: event.data.idCaTipoEmail,
      idCaPessoa: idCaPessoa,
      principal: checked ? "F" : "T",
      email: event.data.email,
      dataInclusao: event.data.dataInclusao,
    };
    handleUpdateEmail(params)
      .then((res) => {
        event.data.principal = checked ? "T" : "F";
        if (event.data.principal) {
          listIds.push({
            id: res.idCaPessoaEmail,
          });
          setListIds(listIds);
        }

        addToast(message, {
          appearance: "success",
          autoDismiss: true,
          transitionState: "entered",
        });
      })
      .catch((error) => {
        addToast(
          handleErrorForm(
            `Algo de errado aconteceu, não foi possível ${
              checked ? "desmarcar" : "marcar"
            }`,
            error
          ),
          {
            appearance: "error",
            autoDismiss: true,
            transitionState: "entered",
          }
        );
      });
  };
  const checkRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          checked={e.data.principal === "T" ? true : false}
          onCheckboxClick={() => handleSelectionChanged(e)}
        />
      </div>
    );
  };

  const editCheckRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          disabled={true}
          checked={e.data.principal === "T" ? true : false}
        />
      </div>
    );
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourceEmail(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowValidating={handleInsertError}
        onOptionChanged={handleOptionChange}
        style={{ marginTop: 20, width: "65%" }}
        loadPanel={false}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idCaTipoEmail"
          caption="Tipo Email"
          filterOperations={false}
          width={"30%"}
        >
          <Lookup
            dataSource={gridDataSourceEmailType}
            displayExpr="descricao"
            valueExpr="idCaTipoEmail"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="email"
          caption="Email"
          filterOperations={false}
          width={"40%"}
        >
          <RequiredRule />
          <EmailRule />
        </Column>

        <Column
          alignment="center"
          dataField="principal"
          caption={"Principal"}
          cellRender={checkRender}
          editCellRender={editCheckRender}
          width={"10%"}
        ></Column>
      </DataGrid>
    </div>
  );
};

export default EmailGrid;
