import React, { useState } from "react";
import {
  Column,
  Button,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
} from "devextreme-react/data-grid";

import {
  gridDataSourcePerson,
  getFoto,
} from "../../../store/modules/person/action";

import { handleLoadDadosAdditional } from "../PessoaServices";

const PersonFilterGrid = ({
  setForm,
  setAdditionalForm,
  initialAdditionalForm,
  setToggle,
  setFiles,
  setActiveTab,
  warningMessage,
}) => {
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function getDadosAdditional(idCaPessoa) {
    handleLoadDadosAdditional(idCaPessoa)
      .then((res) => setAdditionalForm({ ...initialAdditionalForm, ...res[0] }))
      .catch((error) => warningMessage("Erro ao carregar dados adicionais"));
  }

  return (
    <>
      <DataGrid
        dataSource={gridDataSourcePerson}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onOptionChanged={handleOptionChange}
        height={400}
        style={{ marginTop: 20 }}
      >
        <Editing mode="row" useIcons allowDeleting />
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="nomeRazaoSocial"
          caption="Razão Social / Nome"
          filterOperations={false}
          allowEditing={false}
        >
          <HeaderFilter groupInterval={70} />
        </Column>
        <Column
          dataField="nomeSocialFantasia"
          caption="Nome Social / Fantasia "
          filterOperations={false}
          allowEditing={false}
        >
          <HeaderFilter groupInterval={70} />
        </Column>
        <Column
          dataField="cpfCnpj"
          caption="CPF/CNPJ"
          filterOperations={false}
          allowEditing={false}
        >
          <HeaderFilter groupInterval={70} />
        </Column>
        <Column type="buttons" width={130}>
          <Button
            hint="Editar"
            visible
            icon="edit"
            onClick={(value) => {
              const data = value?.row?.data;
              setFiles({});
              setForm({
                idCaPessoa: data.idCaPessoa,
                tipoPessoa: data.tipoPessoa,
                nomeRazaoSocial: data.nomeRazaoSocial,
                nomeSocialFantasia: data.nomeSocialFantasia,
                cpfCnpj: data.cpfCnpj,
                urlFotoLogotipo: data.urlFotoLogotipo,
                extensaoArquivoFotoLogo: data.extensaoArquivoFotoLogo,
                nomeArquivoFotoLogo: data.nomeArquivoFotoLogo,
              });
              setToggle(true);
              getDadosAdditional(data.idCaPessoa);
              getFoto(data.idCaPessoa, setFiles);
              setActiveTab();
            }}
          />
          <Button name="delete" visible hint="Deletar" icon="remove" />
        </Column>
      </DataGrid>
    </>
  );
};

export default PersonFilterGrid;
