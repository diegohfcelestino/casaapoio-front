import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
  MasterDetail
} from "devextreme-react/data-grid";
import DocumentDetailGrid from "./DocumentDetailGrid";

import {
  selectDetail,
  gridDataSourceDocument,
  gridDataSourceDocumentType,
} from "../../../store/modules/person/action";

const DocumentGrid = ({ idCaPessoa, successMessage, errorMessage }) => {
  const dispatch = useDispatch();
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleInsertError(event) {
    const { newData, oldData = false } = event;
    newData.documento = newData.documento ?? oldData.documento;
    newData.idTipoDocumento =
      newData.idTipoDocumento ?? oldData.idTipoDocumento;
    newData.dataInclusao = oldData.dataInclusao;
    newData.usuario = oldData.usuario;
  }

  function handleRemove({ data }) {
    successMessage("Documento deletado com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourceDocument(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowValidating={handleInsertError}
        onOptionChanged={handleOptionChange}
        style={{ marginTop: 20, width: "65%" }}
      >
        <MasterDetail enabled={true} component={DocumentDetailGrid} />
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idTipoDocumento"
          caption="Tipo Documento"
          filterOperations={false}
          width={"35%"}
        >
          <Lookup
            dataSource={gridDataSourceDocumentType}
            displayExpr="descricao"
            valueExpr="idCaTipoDocumento"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="documento"
          caption="Documento"
          filterOperations={false}
          width={"50%"}
        >
          <RequiredRule />
        </Column>


      </DataGrid>
    </div>
  );
};

export default DocumentGrid;
