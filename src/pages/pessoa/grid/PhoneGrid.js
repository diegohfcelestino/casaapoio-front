import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
} from "devextreme-react/data-grid";
import { handleUpdatePhone } from "../PessoaServices";
import { Checkbox } from "rsuite";
import { useToasts } from "react-toast-notifications";
import { handleErrorForm } from "../../../services/helper";
import {
  gridDataSourcePhone,
  gridDataSourcePhoneType,
} from "../../../store/modules/person/action";
import Cleave from "cleave.js/react";

const PhoneGrid = ({ idCaPessoa, successMessage, errorMessage }) => {
  const { addToast } = useToasts();
  const [listIds, setListIds] = useState([]);
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleRemove({ data }) {
    successMessage("Tipo Telefone deletada com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  function handleInsertError(event) {
    const { newData, oldData = false } = event;

    newData.telefone = newData.telefone ?? oldData.telefone;
    newData.idTipoTelefone = newData.idTipoTelefone ?? oldData.idTipoTelefone;
    newData.dataInclusao = oldData.dataInclusao;
    newData.principal = oldData.principal ?? "F";
  }

  const handleSelectionChanged = (event) => {
    let checked = event.data.principal === "T";
    let message = `Telefone   ${
      !checked ? "marcado" : "desmarcado"
    } como principal`;
    let id = 0;
    listIds.map((item) => {
      if (
        event.data.idCaPessoaTelefone === item.idCaPessoaTelefone &&
        checked
      ) {
        id = item.id;
      }
      return item;
    });

    let params = {
      idCaPessoaTelefone: id ? id : event.data.idCaPessoaTelefone,
      idTipoTelefone: event.data.idTipoTelefone,
      idCaPessoa: idCaPessoa,
      principal: checked ? "F" : "T",
      telefone: event.data.telefone,
      dataInclusao: event.data.dataInclusao,
    };
    handleUpdatePhone(params)
      .then((res) => {
        event.data.principal = checked ? "T" : "F";
        if (event.data.principal) {
          listIds.push({
            id: res.idCaPessoaTelefone,
          });
          setListIds(listIds);
        }

        addToast(message, {
          appearance: "success",
          autoDismiss: true,
          transitionState: "entered",
        });
      })
      .catch((error) => {
        addToast(
          handleErrorForm(
            `Algo de errado aconteceu, não foi possível ${
              checked ? "desmarcar" : "marcar"
            }`,
            error
          ),
          {
            appearance: "error",
            autoDismiss: true,
            transitionState: "entered",
          }
        );
      });
  };
  const checkRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          checked={e.data.principal === "T" ? true : false}
          onCheckboxClick={() => handleSelectionChanged(e)}
        />
      </div>
    );
  };

  const editCheckRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          disabled={true}
          checked={e.data.principal === "T" ? true : false}
        />
      </div>
    );
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourcePhone(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onRowInserted={handleInsert}
        onRowValidating={handleInsertError}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onOptionChanged={handleOptionChange}
        style={{ marginTop: 20, width: "65%" }}
        loadPanel={false}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="idTipoTelefone"
          caption="Tipo Telefone"
          filterOperations={false}
          width={"30%"}
        >
          <Lookup
            dataSource={gridDataSourcePhoneType}
            displayExpr="descricao"
            valueExpr="idCaTipoTelefone"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="telefone"
          caption="Telefone"
          filterOperations={false}
          width={"40%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask"
              options={{
                numericOnly: true,
                phone: true,
                phoneRegionCode: "BR",
              }}
              onChange={(e) =>
                rowData.data.setValue(e.target.value, "telefone")
              }
              value={rowData.data.value}
            ></Cleave>
          )}
        >
          <RequiredRule />
        </Column>

        <Column
          alignment="center"
          dataField="principal"
          caption={"Principal"}
          cellRender={checkRender}
          editCellRender={editCheckRender}
          width={"10%"}
        ></Column>
      </DataGrid>
    </div>
  );
};

export default PhoneGrid;
