import React, { useState } from "react";
import {
  Column,
  DataGrid,
  HeaderFilter,
  Pager,
  Paging,
  FilterRow,
} from "devextreme-react/data-grid";
import { handleUpdateNecessidadePrecaucao } from "../PessoaServices";
import { Checkbox } from "rsuite";
import { useToasts } from "react-toast-notifications";
import { gridDataSourceNecessidadePrecaucao } from "../../../store/modules/person/action";
import { handleErrorForm } from "../../../services/helper";

const PrecautionNeedGrid = ({ idCaPessoa }) => {
  const { addToast } = useToasts();
  const [listIds, setListIds] = useState([]);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  const handleSelectionChanged = (event) => {
    let checked = event.data.check === "T";
    let message = `Necessidade ${
      !checked ? "inserido" : "removido"
    } com sucesso`;
    let id = 0;
    listIds.map((item) => {
      if (
        event.data.idCaNecessidadePrecaucao === item.idCaNecessidadePrecaucao &&
        checked
      ) {
        id = item.id;
      }
      return item;
    });
    let params = {
      idCaPessoaNecePrecaucao: id ? id : event.data.id,
      idCaNecessidadePrecaucao: event.data.idNecessidadePrecaucoes,
      idCaPessoa: idCaPessoa,
      check: checked ? "F" : "T",
    };
    handleUpdateNecessidadePrecaucao(params)
      .then((res) => {
        event.data.check = checked ? "T" : "F";
        if (event.data.check) {
          listIds.push({
            id: res.id,
            idCaNecessidadePrecaucao: res.idCaNecessidadePrecaucao,
          });
          setListIds(listIds);
        }

        addToast(message, {
          appearance: "success",
          autoDismiss: true,
          transitionState: "entered",
        });
      })
      .catch((error) => {
        addToast(
          handleErrorForm(
            `Algo de errado aconteceu, não foi possível ${
              checked ? "desmarcar" : "marcar"
            }`,
            error
          ),
          {
            appearance: "error",
            autoDismiss: true,
            transitionState: "entered",
          }
        );
      });
  };
  const checkRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          checked={e.data.check === "T" ? true : false}
          onCheckboxClick={() => handleSelectionChanged(e)}
        />
      </div>
    );
  };
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourceNecessidadePrecaucao(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        style={{ marginTop: 20 }}
        loadPanel={false}
        style={{ marginTop: 20, width: "65%" }}
      >
        <Pager
          visible={true}
          showPageSizeSelector={true}
          allowedPageSizes={[10, 15, 30]}
          showInfo={true}
        />
        <Paging defaultPageSize={15} />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <Column
          dataField="descricaoPrecaucoes"
          caption="Descrição"
          filterOperations={false}
        >
          <HeaderFilter groupInterval={100} />
        </Column>

        <Column
          alignment="center"
          dataField="check"
          caption={"Possui?"}
          cellRender={checkRender}
          width={"20%"}
        ></Column>
      </DataGrid>
    </div>
  );
};

export default PrecautionNeedGrid;
