import React, { useState } from "react";
import {
  Column,
  DataGrid,
  HeaderFilter,
  Pager,
  Paging,
  FilterRow,
} from "devextreme-react/data-grid";
import { handleUpdateNecessidadeEspecial } from "../PessoaServices";
import { Checkbox } from "rsuite";
import { useToasts } from "react-toast-notifications";
import { gridDataSourceNecessidadeEspecial } from "../../../store/modules/person/action";
import { handleErrorForm } from "../../../services/helper";

const SpecialNeedGrid = ({ idCaPessoa }) => {
  const { addToast } = useToasts();
  const [listIds, setListIds] = useState([]);

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  const handleSelectionChanged = (event) => {
    let checked = event.data.check === "T";
    let message = `Necessidade ${
      !checked ? "inserido" : "removido"
    } com sucesso`;
    let id = 0;
    listIds.map((item) => {
      if (
        event.data.idNecessidadeEspecial === item.idNecessidadeEspecial &&
        checked
      ) {
        id = item.id;
      }
      return item;
    });

    let params = {
      idCaPessoaNecesEspecial: id ? id : event.data.id,
      idNecessidadeEspecial: event.data.idNecessidadeEspecial,
      idCaPessoa: idCaPessoa,
      check: checked ? "F" : "T",
    };
    handleUpdateNecessidadeEspecial(params)
      .then((res) => {
        event.data.check = checked ? "T" : "F";
        if (event.data.check) {
          listIds.push({
            id: res.id,
            idNecessidadeEspecial: res.idNecessidadeEspecial,
          });
          setListIds(listIds);
        }

        addToast(message, {
          appearance: "success",
          autoDismiss: true,
          transitionState: "entered",
        });
      })
      .catch((error) => {
        addToast(
          handleErrorForm(
            `Algo de errado aconteceu, não foi possível ${
              checked ? "desmarcar" : "marcar"
            }`,
            error
          ),
          {
            appearance: "error",
            autoDismiss: true,
            transitionState: "entered",
          }
        );
      });
  };
  const checkRender = (e) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Checkbox
          checked={e.data.check === "T" ? true : false}
          onCheckboxClick={() => handleSelectionChanged(e)}
        />
      </div>
    );
  };
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <DataGrid
        dataSource={gridDataSourceNecessidadeEspecial(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        style={{ marginTop: 20 }}
        loadPanel={false}
        style={{ marginTop: 20, width: "65%" }}
      >
        <Pager
          visible={true}
          showPageSizeSelector={true}
          allowedPageSizes={[10, 15, 30]}
          showInfo={true}
        />
        <Paging defaultPageSize={15} />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <Column
          dataField="descricaoNecessidade"
          caption="Descrição"
          filterOperations={false}
        >
          <HeaderFilter groupInterval={100} />
        </Column>

        <Column
          alignment="center"
          dataField="check"
          caption={"Possui?"}
          cellRender={checkRender}
          width={"20%"}
        ></Column>
      </DataGrid>
    </div>
  );
};

export default SpecialNeedGrid;
