import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  Column,
  DataGrid,
  Editing,
  HeaderFilter,
  FilterRow,
  Pager,
  Paging,
  Lookup,
  RequiredRule,
} from "devextreme-react/data-grid";

import ModalSelect from "../../../components/modalSelect/ModalSelect";

import {
  cboStore,
  naturalidadeStore,
  nacionalidadeStore,
  getParentesco,
} from "../PessoaServices";

import Cleave from "cleave.js/react";

import { gridDataSource as ReligionStore } from "../../../store/modules/religion/action";

import { gridDataSourceFamilyComposition } from "../../../store/modules/person/action";

const FamilyCompositionGrid = ({
  idCaPessoa,
  successMessage,
  errorMessage,
}) => {
  const dispatch = useDispatch();
  const [pageIndex, setPageIndex] = useState(null);
  const [pageSize, setPageSize] = useState(15);

  const [key, setKey] = useState(null);

  const [idProfissao, setIdProfissao] = useState(null);
  const [idNaturalidade, setIdNaturalidade] = useState(null);
  const [idNacionalidade, setIdNacionalidade] = useState(null);
  const [idReligiao, setIdReligiao] = useState(null);

  const [listProfissao, setListProfissao] = useState([]);
  const [listNaturalidade, setListNaturalidade] = useState([]);
  const [listNacionalidade, setListNacionalidade] = useState([]);
  const [listReligiao, setListReligiao] = useState([]);

  const [toggleProfissao, setToggleProfissao] = useState(false);
  const [toggleNaturalidade, setToggleNaturalidade] = useState(false);
  const [toggleNacionalidade, setToggleNacionalidade] = useState(false);
  const [toggleReligiao, setToggleReligiao] = useState(false);

  const [listPrecaucao, setListPrecaucao] = useState([]);
  const [descricaoPrecaucao, setDescricaoPrecaucao] = useState("");

  useEffect(() => {
    getParentesco(descricaoPrecaucao, setListPrecaucao);
  }, []);

  function toggleModalNacionalidade(data) {
    setToggleNacionalidade(!toggleNacionalidade);
    if (!data) return;
    if (
      data.idCaPessoaCompoFamiliar !== undefined ||
      data.idCaPessoaCompoFamiliar !== null
    ) {
      setKey(data.idCaPessoaCompoFamiliar);
    }

    // Pega item selecionado na modal
    if (data?.idCrPais) {
      listNacionalidade.map((item) => {
        if (item.idCaPessoaCompoFamiliar === key) {
          item.idNacionalidade = data.idCrPais;
          item.descricaoNacionalidade = data.nomepais;
          setIdNacionalidade(data.idCrPais);
          setListNacionalidade([...listNacionalidade]);
        }

        return item;
      });

      setListNacionalidade([...listNacionalidade]);
    }
  }
  function toggleModalNaturalidade(data) {
    setToggleNaturalidade(!toggleNaturalidade);
    if (!data) return;
    if (
      data.idCaPessoaCompoFamiliar !== undefined ||
      data.idCaPessoaCompoFamiliar !== null
    ) {
      setKey(data.idCaPessoaCompoFamiliar);
    }

    // Pega item selecionado na modal
    if (data?.idCrCidades) {
      listNaturalidade.map((item) => {
        if (item.idCaPessoaCompoFamiliar === key) {
          item.idNaturalidade = data.idCrCidades;
          item.descricaoNaturalidade = data.cidade;
          setIdNaturalidade(data.idCrCidades);
          setListNaturalidade([...listNaturalidade]);
        }

        return item;
      });

      setListNaturalidade([...listNaturalidade]);
    }
  }
  function toggleModalProfissao(data) {
    setToggleProfissao(!toggleProfissao);
    if (!data) return;
    if (
      data.idCaPessoaCompoFamiliar !== undefined ||
      data.idCaPessoaCompoFamiliar !== null
    ) {
      setKey(data.idCaPessoaCompoFamiliar);
    }

    // Pega item selecionado na modal
    if (data?.idFpCbo) {
      listProfissao.map((item) => {
        if (item.idCaPessoaCompoFamiliar === key) {
          item.idProfissao = data.idFpCbo;
          item.descricaoProfissao = data.descricao;
          setIdProfissao(data.idFpCbo);
          setListProfissao([...listProfissao]);
        }

        return item;
      });

      setListProfissao([...listProfissao]);
    }
  }
  function toggleModalReligiao(data) {
    setToggleReligiao(!toggleReligiao);
    if (!data) return;
    if (
      data.idCaPessoaCompoFamiliar !== undefined ||
      data.idCaPessoaCompoFamiliar !== null
    ) {
      setKey(data.idCaPessoaCompoFamiliar);
    }

    // Pega item selecionado na modal
    if (data?.idCaReligiao) {
      listReligiao.map((item) => {
        if (item.idCaPessoaCompoFamiliar === key) {
          item.idReligiao = data.idCaReligiao;
          item.descricaoReligiao = data.nome;
          setIdReligiao(data.idCaReligiao);
          setListReligiao([...listReligiao]);
        }

        return item;
      });

      setListReligiao([...listReligiao]);
    }
  }

  function handleInputModal(event, toggleModal) {
    return (
      <div
        style={{
          cursor: "pointer",
          height: 22,
          display: "flex",
        }}
      >
        <div
          className="ellipsis"
          onClick={() => {
            toggleModal({
              idCaPessoaCompoFamiliar: event.data.idCaPessoaCompoFamiliar || 0,
            });
          }}
          style={{ width: "100%", paddingLeft: "7px" }}
        >
          {event.value ?? "Clique para selecionar"}
        </div>
      </div>
    );
  }

  function editCellNacionalidade(event) {
    let { data } = event.data;

    listNacionalidade.map((item) => {
      const checkIfHasId = item.idCaPessoaCompoFamiliar !== undefined;
      const checkIds =
        item.idCaPessoaCompoFamiliar === data.idCaPessoaCompoFamiliar ||
        item.idCaPessoaCompoFamiliar === data.id;

      if (checkIfHasId && checkIds) {
        data.idNacionalidade = item.idNacionalidade ?? data.idNacionalidade;
        data.descricaoNacionalidade =
          item.descricaoNacionalidade ?? data.descricaoNacionalidade;
        data.value = item.descricaoNacionalidade;
        event.data.setValue(
          item.descricaoNacionalidade,
          "descricaoNacionalidade"
        );
      }

      return item;
    });

    return handleInputModal(event.data, toggleModalNacionalidade);
  }
  function editCellNaturalidade(event) {
    let { data } = event.data;

    listNaturalidade.map((item) => {
      const checkIfHasId = item.idCaPessoaCompoFamiliar !== undefined;
      const checkIds =
        item.idCaPessoaCompoFamiliar === data.idCaPessoaCompoFamiliar ||
        item.idCaPessoaCompoFamiliar === data.id;

      if (checkIfHasId && checkIds) {
        data.idCaPessoaCompoFamiliar =
          item.idCaPessoaCompoFamiliar ?? data.idCaPessoaCompoFamiliar;
        data.idNaturalidade = item.idNaturalidade ?? data.idNaturalidade;
        data.descricaoNaturalidade =
          item.descricaoNaturalidade ?? data.descricaoNaturalidade;
        data.value = item.descricaoNaturalidade;
        event.data.setValue(
          item.descricaoNaturalidade,
          "descricaoNaturalidade"
        );
      }

      return item;
    });

    return handleInputModal(event.data, toggleModalNaturalidade);
  }
  function editCellProfissao(event) {
    const { data } = event.data;

    listProfissao.map((item) => {
      const checkIfHasId = item.idCaPessoaCompoFamiliar !== undefined;
      const checkIds =
        item.idCaPessoaCompoFamiliar === data.idCaPessoaCompoFamiliar ||
        item.idCaPessoaCompoFamiliar === data.id;

      if (checkIfHasId && checkIds) {
        data.idProfissao = item.idProfissao ?? data.idProfissao;
        data.descricaoProfissao =
          item.descricaoProfissao ?? data.descricaoProfissao;
        data.value = item.descricaoProfissao;
        event.data.setValue(item.descricaoProfissao, "descricaoProfissao");
      }

      return item;
    });

    return handleInputModal(event.data, toggleModalProfissao);
  }
  function editCellReligiao(event) {
    const { data } = event.data;

    listReligiao.map((item) => {
      const checkIfHasId = item.idCaPessoaCompoFamiliar !== undefined;
      const checkIds =
        item.idCaPessoaCompoFamiliar === data.idCaPessoaCompoFamiliar ||
        item.idCaPessoaCompoFamiliar === data.id;

      if (checkIfHasId && checkIds) {
        data.idReligiao = item.idReligiao ?? data.idReligiao;
        data.descricaoReligiao =
          item.descricaoReligiao ?? data.descricaoReligiao;
        data.value = item.descricaoReligiao;
        event.data.setValue(item.descricaoReligiao, "descricaoReligiao");
      }

      return item;
    });

    return handleInputModal(event.data, toggleModalReligiao);
  }

  const applyFilterTypes = [
    {
      key: "auto",
      name: "Immediately",
    },
    {
      key: "onClick",
      name: "On Button Click",
    },
  ];
  const filterOptions = {
    showFilterRow: true,
    showHeaderFilter: true,
    currentFilter: applyFilterTypes[0].key,
  };

  function handleOptionChange(event) {
    if (event.fullName === "paging.pageSize" && event.value !== pageSize) {
      setPageSize(event.value);
      setPageIndex(0);
    }

    setPageIndex(null);
  }

  function handleNewRow(event) {
    event.data.idCaPessoaCompoFamiliar = 0;
    listProfissao.push({ idCaPessoaCompoFamiliar: 0 });
    setListProfissao([...listProfissao]);

    listNaturalidade.push({ idCaPessoaCompoFamiliar: 0 });
    setListNaturalidade([...listNaturalidade]);

    listNacionalidade.push({ idCaPessoaCompoFamiliar: 0 });
    setListNacionalidade([...listNacionalidade]);

    listReligiao.push({ idCaPessoaCompoFamiliar: 0 });
    setListReligiao([...listReligiao]);
  }

  function handleEditingStart(event) {
    listProfissao.push({
      idCaPessoaCompoFamiliar: event.data.idCaPessoaCompoFamiliar,
      descricaoProfissao: event.data.descricaoProfissao,
    });
    setListProfissao([...listProfissao]);

    listNaturalidade.push({
      idCaPessoaCompoFamiliar: event.data.idCaPessoaCompoFamiliar,
      descricaoNaturalidade: event.data.descricaoNaturalidade,
    });
    setListNaturalidade([...listNaturalidade]);

    listNacionalidade.push({
      idCaPessoaCompoFamiliar: event.data.idCaPessoaCompoFamiliar,
      descricaoNacionalidade: event.data.descricaoNacionalidade,
    });
    setListNacionalidade([...listNacionalidade]);

    listReligiao.push({
      idCaPessoaCompoFamiliar: event.data.idCaPessoaCompoFamiliar,
      descricaoReligiao: event.data.descricaoReligiao,
    });
    setListReligiao([...listReligiao]);
  }

  function handleInsert(event) {
    successMessage("Cadastro feito com sucesso");
  }

  function handleInsertError(event) {
    const { newData, oldData = false } = event;

    newData.idParentesco = newData.idParentesco ?? oldData.idParentesco;
    newData.nome = newData.nome ?? oldData.nome;
    newData.nascimento = newData.nascimento ?? oldData.nascimento;
    newData.rg = newData.rg ?? oldData.rg;
    newData.cpf = newData.cpf ?? oldData.cpf;
    newData.idNacionalidade = idNacionalidade ?? oldData.idNacionalidade;
    newData.idProfissao = idProfissao ?? oldData.idProfissao;
    newData.idReligiao = idReligiao ?? oldData.idReligiao;
    newData.idNaturalidade = idNaturalidade ?? oldData.idNaturalidade;
    newData.descricaoNacionalidade = idNacionalidade ?? oldData.idNacionalidade;
    newData.descricaoProfissao = idProfissao ?? oldData.idProfissao;
    newData.descricaoReligiao = idReligiao ?? oldData.idReligiao;
    newData.descricaoNaturalidade = idNaturalidade ?? oldData.idNaturalidade;
    newData.dataInclusao = oldData.dataInclusao;
    newData.usuario = oldData.usuario;
  }

  function handleRemove({ data }) {
    successMessage("Deletado com sucesso");
  }

  function handleUpdate(event) {
    successMessage("Alteração feita com sucesso");
  }

  return (
    <>
      <ModalSelect
        dataSource={nacionalidadeStore}
        colums={[
          {
            dataField: "nomepais",
            caption: "País",
            width: "100%",
          },
        ]}
        title={"Nacionalidade"}
        visible={toggleNacionalidade}
        toggleModal={toggleModalNacionalidade}
        width={"40%"}
      />
      <ModalSelect
        dataSource={naturalidadeStore}
        colums={[
          {
            dataField: "cidade",
            caption: "Cidade",
            width: "70%",
          },
          {
            dataField: "uf",
            caption: "UF",
            width: "10%",
          },
          {
            dataField: "codigo",
            caption: "Código IBGE",
            alignment: "rigth",
          },
        ]}
        title={"Naturalidade"}
        visible={toggleNaturalidade}
        toggleModal={toggleModalNaturalidade}
        width={"60%"}
      />
      <ModalSelect
        dataSource={cboStore}
        colums={[
          {
            dataField: "descricao",
            caption: "Profissão",
            width: "70%",
          },
          {
            dataField: "cbo",
            caption: "CBO",
          },
        ]}
        title={"Profissão"}
        visible={toggleProfissao}
        toggleModal={toggleModalProfissao}
        width={"50%"}
      />
      <ModalSelect
        dataSource={ReligionStore}
        colums={[
          {
            dataField: "nome",
            caption: "Religião",
          },
        ]}
        title={"Religião"}
        visible={toggleReligiao}
        toggleModal={toggleModalReligiao}
        width={"30%"}
      />
      <DataGrid
        dataSource={gridDataSourceFamilyComposition(idCaPessoa)}
        remoteOperations={true}
        showBorders={true}
        allowColumnReordering={true}
        allowColumnResizing={true}
        columnAutoWidth={true}
        onInitNewRow={handleNewRow}
        onEditingStart={handleEditingStart}
        onRowInserted={handleInsert}
        onRowRemoved={handleRemove}
        onRowUpdated={handleUpdate}
        onRowValidating={handleInsertError}
        onOptionChanged={handleOptionChange}
        style={{ marginTop: 20 }}
        loadPanel={false}
      >
        <Pager
          allowedPageSizes={[7, 15, 30]}
          visible={true}
          showPageSizeSelector={true}
          showInfo={true}
        />
        <Paging defaultPageSize={pageSize} pageIndex={pageIndex} />
        <Editing mode="row" allowAdding allowDeleting allowUpdating useIcons />
        <FilterRow
          visible={filterOptions.showFilterRow}
          applyFilter={filterOptions.currentFilter}
        />
        <HeaderFilter />

        <Column
          dataField="nome"
          caption="Nome"
          filterOperations={false}
          width={"15%"}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="nascimento"
          caption="Data Nascimento"
          dataType="date"
          format="dd/MM/yyyy"
          alignment="rigth"
          width={"10%"}
          allowEditing={true}
          filterOperations={false}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="rg"
          caption="RG"
          filterOperations={false}
          width={"8%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask"
              options={{
                blocks: [2, 3, 3, 1],
                delimiters: [".", ".", "-"],
                numericOnly: true,
              }}
              value={rowData.data.value}
              onChange={(event) =>
                rowData.data.setValue(event.target.rawValue, "rg")
              }
            />
          )}
        ></Column>

        <Column
          dataField="cpf"
          caption="CPF"
          filterOperations={false}
          width={"8%"}
          editCellComponent={(rowData) => (
            <Cleave
              className="gridInputMask"
              options={{
                blocks: [3, 3, 3, 2],
                delimiters: [".", ".", "-"],
                //numericOnly: true,
              }}
              value={rowData.data.value}
              onChange={(event) =>
                rowData.data.setValue(event.target.rawValue, "cpf")
              }
            />
          )}
        ></Column>

        <Column
          dataField="idParentesco"
          caption="Parentesco"
          filterOperations={false}
          width={"7%"}
        >
          <Lookup
            dataSource={listPrecaucao}
            displayExpr="descricao"
            valueExpr="idDpParentesco"
          />
          <RequiredRule />
        </Column>

        <Column
          dataField="descricaoNacionalidade"
          caption="Nacionalidade"
          filterOperations={false}
          width={"10%"}
          editCellComponent={editCellNacionalidade}
        >
          <RequiredRule />
        </Column>

        <Column
          dataField="descricaoNaturalidade"
          caption="Naturalidade"
          filterOperations={false}
          width={"12%"}
          editCellComponent={editCellNaturalidade}
        ></Column>

        <Column
          dataField="descricaoProfissao"
          caption="Profissão"
          filterOperations={false}
          width={"15%"}
          editCellComponent={editCellProfissao}
        ></Column>

        <Column
          dataField="descricaoReligiao"
          caption="Religião"
          filterOperations={false}
          editCellComponent={editCellReligiao}
        ></Column>
      </DataGrid>
    </>
  );
};

export default FamilyCompositionGrid;
