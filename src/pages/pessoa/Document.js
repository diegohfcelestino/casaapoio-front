import React, { useMemo, useEffect } from "react";
import Uppy from "@uppy/core";
import { DashboardModal } from "@uppy/react";
import pt_BR from "@uppy/locales/lib/pt_BR";
import Webcam from "@uppy/webcam";
import { handlePostDocumentoImg } from "./PessoaServices";

import { useRef } from "react";

const Document = ({
  idCaPessoaDocumenImagem,
  count,
  getFileList,
  modalOpen,
  handleModal,
  warningMessage,
  successMessage,
  errorMessage,
}) => {
  const timeoutRef = useRef(null);

  useEffect(() => {
    if (modalOpen) {
      navigator.mediaDevices.getUserMedia({ video: true }).catch((err) => {
        warningMessage(
          "Não foi possível acessar webcan, verifique permissão do navegador"
        );
      });
    }
  }, [modalOpen]);

  const uppy = useMemo(() => {
    return Uppy({
      locale: pt_BR,
      restrictions: {
        maxFileSize: 1000000000000, //até 1GB
        maxNumberOfFiles: count ? 5 - count : 5,
        allowedFileTypes: [".png", ".jpg", ".jpeg", ".pdf"],
      },
    }).use(Webcam, {
      onBeforeSnapshot: () => Promise.resolve(),
      countdown: false,
      modes: ["picture"],
    });
  }, [count]);

  uppy.on("file-added", (file) => {
    if (file.name.length > 50) {
      //valida tamanho nome do arquivo
      handleModal(false);
      uppy.removeFile(file.id);
      warningMessage("Nome do arquivo deve ser de até 50 caracteres!");
    }
  });
  uppy.on("upload", (e) => {
    let dataFiles = e.fileIDs.map((id) => uppy.getFile(id));
    let files = dataFiles.map((file) => file.data);
    if (files) {
      window.clearTimeout(timeoutRef.current);
      timeoutRef.current = window.setTimeout(() => {
        handlePostDocumentoImg(idCaPessoaDocumenImagem, files)
          .then((res) => {
            successMessage(res);
            getFileList();
            handleModal(false);
            uppy.reset();
          })
          .catch((err) => errorMessage(err));
      }, 500);
    }
  });
  uppy.on("complete", (result) => {
    console.log(
      "successful files:",
      result.successful,
      "failed files:",
      result.failed
    );
    result?.failed && errorMessage(`Arquivos não enviados: ${result.failed}`);
    uppy.reset();
  });

  return (
    <>
      {modalOpen && (
        <DashboardModal
          uppy={uppy}
          //closeAfterFinish
          closeModalOnClickOutside
          allowMultipleUploads={true}
          allowNewUpload={true}
          open={modalOpen}
          onRequestClose={() => handleModal(false)}
          plugins={["Webcam"]}
          hideUploadButton={false}
          hideCancelButton={true}
          hideRetryButton={true}
          locale={{
            strings: {
              // Used as the label for the link that opens the system file selection dialog.
              browse: "procurar no dispostivo",
            },
          }}
        />
      )}
    </>
  );
};

export default Document;
