import React, { useMemo } from "react";
import { Nav } from "rsuite";

import "../../Page.css";

import DadosPessoaisForm from "../form/DadosPessoaisForm";
import DadosAdicionaisForm from "../form/DadosAdicionaisForm";

const PessoaTab = (formProps) => {
  const CustomNav = ({ active, onSelect, ...props }) => {
    return (
      <Nav {...props} activeKey={active} onSelect={onSelect}>
        <Nav.Item disabled={!formProps.editing} eventKey="DadosPessoaisForm">
          Dados Pessoais
        </Nav.Item>
        {formProps.personFisic && (
          <Nav.Item
            disabled={!formProps.editing}
            eventKey="DadosAdicionaisForm"
          >
            Dados Adicionais
          </Nav.Item>
        )}
      </Nav>
    );
  };

  const customForm = useMemo(() => {
    const tabs = {
      DadosPessoaisForm: DadosPessoaisForm,
      DadosAdicionaisForm: DadosAdicionaisForm,
    };

    const CustomComponent = tabs[formProps.activeTab];
    return CustomComponent ? (
      <CustomComponent
        setActiveTab={() => formProps.setActiveTab("DadosPessoaisForm")}
        {...formProps}
      />
    ) : null;
  }, [formProps]);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <CustomNav
        appearance="tabs"
        active={formProps.activeTab}
        onSelect={(newTab) => formProps.setActiveTab(newTab)}
      />
      {customForm}
    </div>
  );
};

export default PessoaTab;
