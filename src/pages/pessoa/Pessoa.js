import { useState } from "react";
import { useToasts } from "react-toast-notifications";
import FilterModal from "../../components/filter/FilterModal";
import PersonNavForm from "./tab/PessoaTab";
import PersonFilterGrid from "./grid/PersonFilterGrid";

import "../Page.css";

const Pessoa = () => {
  const { addToast } = useToasts();

  const initialForm = {
    idCaPessoa: 0,
    tipoPessoa: "J", //1
    nomeRazaoSocial: "", //70
    nomeSocialFantasia: "", //70
    cpfCnpj: "", //15
    urlFotoLogotipo: "", //100
    nomeArquivoFotoLogo: "",
    extensaoArquivoFotoLogo: "",
  };

  const initialAdditionalForm = {
    idCaPessoaDadosAdic: 0,
    empresa: 0,
    idCaPessoa: 0,
    sexo: "",
    nascimento: null,
    idNaturalidade: null,
    idNacionalidade: null,
    idEstadoCivil: null,
    idEscolaridade: null,
    idProfissao: null,
    idNumeroCalcado: null,
    idTamanhoCamisa: null,
    idTamanhoCalca: null,
    idReligiao: null,
    formacao: "",
    dataInclusao: new Date(),
    usuario: "",
    excluido: "",
    descricaoNacionalidade: "",
    descricaoNaturalidade: "",
    descricaoNumeroCalcado: "",
    descricaoProfissao: "",
    descricaoReligiao: "",
    descricaoTamanhoCalca: "",
    descricaoTamanhoCamisa: "",
  };

  const [form, setForm] = useState(initialForm);
  const [additionalForm, setAdditionalForm] = useState(initialAdditionalForm);

  const [toggle, setToggle] = useState(false);

  const [files, setFiles] = useState({});
  //const [loadingFile, setLoadingFile] = useState(false);

  const [activeTab, setActiveTab] = useState("primaryForm");

  function successMessage(message) {
    addToast(message, {
      appearance: "success",
      autoDismiss: true,
    });
  }

  function warningMessage(message) {
    addToast(message, {
      appearance: "warning",
      autoDismiss: true,
    });
  }

  function errorMessage(message) {
    addToast(message, {
      appearance: "error",
      autoDismiss: true,
    });
  }

  return (
    <div className="pageContainer">
      <div className="pageFlex">
        <div className="pageContent withoutSearch w2000">
          <h4 className="margin-bottom">Cadastro Pessoas</h4>
          <PersonNavForm
            editing={form.idCaPessoa}
            personFisic={form.tipoPessoa === "F"}
            form={form}
            setForm={setForm}
            additionalForm={additionalForm}
            setAdditionalForm={setAdditionalForm}
            files={files}
            setFiles={setFiles}
            successMessage={successMessage}
            warningMessage={warningMessage}
            errorMessage={errorMessage}
            initialForm={initialForm}
            initialAdditionalForm={initialAdditionalForm}
            activeTab={activeTab}
            setActiveTab={setActiveTab}
          />
        </div>
        <FilterModal toggle={toggle} setToggle={setToggle}>
          <PersonFilterGrid
            form={form}
            setForm={setForm}
            additionalForm={additionalForm}
            setAdditionalForm={setAdditionalForm}
            setFiles={setFiles}
            setToggle={setToggle}
            initialAdditionalForm={initialAdditionalForm}
            setActiveTab={() => setActiveTab("primaryForm")}
            warningMessage={warningMessage}
          />
        </FilterModal>
      </div>
    </div>
  );
};

export default Pessoa;
