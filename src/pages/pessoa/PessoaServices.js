import api from "../../services/api";
import apiXHR from "../../services/apiXHR";
import CustomStore from "devextreme/data/custom_store";
import qs from "qs";
import { handleParams, handleError } from "../../services/helper";
import { Auth } from "../../config/storage";

export function handleLoadDadosAdditional(idPessoa) {
  const params = {
    idPessoa: idPessoa,
  };
  const url = `CaPessoaDadosAdic/GetDadosAdicionaisByPessoa?${qs.stringify(
    params
  )}`;
  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => reject(handleError("Erro ao carregar", err)));
  });
}

function handleLoadCbo(loadOptions) {
  const parametros = {
    descricao: "",
    cbo: "",
    dataInclusao: "",
    skip: 0,
    take: 0,
  };
  const params = handleParams(parametros, loadOptions);
  return api
    .get(`/FpCbo/GetFpCboAutoComplete`, {
      params,
      paramsSerializer: () => qs.stringify(params),
    })
    .then(({ data }) => {
      return { ...data };
    })
    .catch((error) => handleError(error.response));
}

export const cboStore = new CustomStore({
  key: "idFpCbo",
  load: handleLoadCbo,
});

function handleLoadNaturalidade(loadOptions) {
  const parametros = {
    nome: "",
    dataInclusao: "",
    skip: 0,
    take: 0,
  };
  const params = handleParams(parametros, loadOptions);
  return api
    .get(`/CrCidades/GetCrCidadeAutoComplete`, {
      params,
      paramsSerializer: () => qs.stringify(params),
    })
    .then(({ data }) => {
      return { ...data };
    })
    .catch((error) => handleError(error.response));
}

export const naturalidadeStore = new CustomStore({
  key: "idCrCidades",
  load: handleLoadNaturalidade,
});

function handleLoadNacionalidade(loadOptions) {
  const parametros = {
    nome: "",
    dataInclusao: "",
    skip: 0,
    take: 0,
  };
  const params = handleParams(parametros, loadOptions);
  return api
    .get(`/CrPais/GetCrPaisAutoComplete`, {
      params,
      paramsSerializer: () => qs.stringify(params),
    })

    .then(({ data }) => {
      return { ...data };
    })
    .catch((error) => handleError(error.response));
}

export const nacionalidadeStore = new CustomStore({
  key: "idCrPais",
  load: handleLoadNacionalidade,
});

export function getEscolaridade(query, setEscolaridadeList) {
  api
    .get(`/FpGrauInstrucao/GetFpGrauInstrucaoAutoComplete?descricao=${query}`)
    .then((res) => {
      setEscolaridadeList(res.data);
    })
    .catch((error) => handleError(error.response));
}

export function getEstado(query, setEstadoList) {
  api
    .get(`/CrUf/GetCrUfAutoComplete?descricao=${query}`)
    .then((res) => {
      setEstadoList(res.data);
    })
    .catch((error) => handleError(error.response));
}

export function getEstadoCivil(query, setEstadoCivilList) {
  api
    .get(`/FpEstadoCivil/GetFpEstadoCivilAutoComplete?nome=${query}`)
    .then((res) => {
      setEstadoCivilList(res.data);
    })
    .catch((error) => handleError(error.response));
}

export function getTipoLogradouro(query, setTipoLogradouroList) {
  api
    .get(`/FpTipoLogradouro/GetFpTipoLogradouroAutoComplete?descricao=${query}`)
    .then((res) => {
      setTipoLogradouroList(res.data);
    })
    .catch((error) => handleError(error.response));
}

export function getParentesco(query, setParentescoList) {
  api
    .get(`/DpParentesco/GetDpParentescoAutoComplete?descricao=${query}`)
    .then((res) => {
      setParentescoList(res.data);
    })
    .catch((error) => handleError(error.response));
}

export function handleUpdateNecessidadeEspecial(params) {
  const url = "CaPessoaNecesEspecial";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleUpdateNecessidadePrecaucao(params) {
  const url = "CaPessoaNecePrecaucao";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleUpdatePessoaClassificacao(params) {
  const url = "CaPessoaClassificacao/UpdatePessoaClassificacao";
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleUpdateEmail(params) {
  const url = "CaPessoaEmail";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
    excluido: "F",
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleUpdatePhone(params) {
  const url = "CaPessoaTelefone";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
    excluido: "F",
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleUpdateEndereco(params) {
  const url = "CaPessoaEndereco";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...params,
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
    excluido: "F",
  };

  return new Promise((resolve, reject) => {
    api
      .put(url, record)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handlePostDocumentoImg(idCaPessoaDocumento, arquivos) {
  const url = "CaPessoaDocumenImagem";
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idCaPessoaDocumento,
    idCaPessoaDocumenImagem: 0,
    dataInclusao: "",
    usuario: auth.nomeUsuario,
    empresa: auth.empresa,
    excluido: "F",
  };
  var data = new FormData();
  data.append("caPessoaDocumenImagemVo", JSON.stringify(params));
  arquivos.map((item) => data.append("arquivos", item));

  return new Promise((resolve, reject) => {
    apiXHR
      .post(url, data)
      .then((response) => resolve(response.data))
      .catch((error) => reject(handleError(error.response)));
  });
}

export function handleDeleteDocumentoImg(id) {
  //const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    id,
  };
  const url = `CaPessoaDocumenImagem?${qs.stringify(params)}`;

  return new Promise((resolve, reject) => {
    api
      .delete(url)
      .then((response) => resolve(response.data))
      .catch((error) => reject(handleError(error.response)));
  });
}

export function handleGetByIdDocumentoImg(id) {
  const params = {
    id,
  };
  const url = `CaPessoaDocumenImagem/DownloadEVisualizarBase64?${qs.stringify(
    params
  )}`;

  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}

export function handleGetPorPessoaDocumentoImg(id) {
  const params = {
    idCaPessoaDocumento: id,
    skip: 0,
    take: 10,
  };
  const url = `CaPessoaDocumenImagem/GetPorPessoaDocumento?${qs.stringify(
    params
  )}`;

  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error.response));
  });
}
