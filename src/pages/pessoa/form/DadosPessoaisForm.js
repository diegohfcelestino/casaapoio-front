import { useState, useEffect } from "react";
import { cpf, cnpj } from "cpf-cnpj-validator";
import { Form, FlexboxGrid, Button, Schema } from "rsuite";
import { useDispatch } from "react-redux";
import SelectInput from "../../../components/form/selectInput/SelectInput";
import MaskInput from "../../../components/form/maskInput/MaskInput";
import { createPerson } from "../../../store/modules/person/action";

import TextField from "../../../components/textField/TextField";
import PersonNavGrid from "../grid/PersonNavGrid";

const DadosPessoaisForm = ({
  form,
  setForm,
  setAdditionalForm,
  files,
  setFiles,
  ...res
}) => {
  const dispatch = useDispatch();
  const personType = form.tipoPessoa === "F" ? "CPF" : "CNPJ";
  const [errors, setErrors] = useState({});

  const { StringType } = Schema.Types;
  const model = Schema.Model({
    tipoPessoa: StringType().isRequired("Tipo pessoa é requirido"),
    cpfCnpj: StringType()
      .isRequired(`${personType} é requirido`)
      .addRule((value) => {
        return personType === "CPF" ? cpf.isValid(value) : cnpj.isValid(value);
      }),
    nomeRazaoSocial: StringType()
      .isRequired("Razão Social é requirido")
      .maxLength(70, "Razão Social não pode ter mais que 70 caracteres"),
    nomeSocialFantasia: StringType().maxLength(
      50,
      "Nome Fantasia não pode ter mais que 50 caracteres"
    ),
  });

  useEffect(() => {
    if (form.idCaPessoa) {
      setErrors({});
    }
  }, [form]);

  const listTipoPessoa = [
    {
      name: "Física",
      value: "F",
    },
    {
      name: "Jurídica",
      value: "J",
    },
  ];

  function handleChange(value) {
    setForm({ ...form, ...value });
  }

  function handleSubmit(isValid) {
    if (isValid) {
      dispatch(
        createPerson(
          form,
          setForm,
          res.successMessage,
          res.errorMessage,
          res.additionalForm,
          setAdditionalForm,
          files,
          setFiles
        )
      );
    }
  }

  return (
    <>
      <Form
        formValue={form}
        model={model}
        formError={errors}
        onChange={handleChange}
        onSubmit={handleSubmit}
        onCheck={setErrors}
        fluid
        style={{ marginTop: 20 }}
      >
        <div className="primaryForm">
          <div className="primaryFormContainer">
            <FlexboxGrid>
              <SelectInput
                label="Tipo Pessoa"
                error="Por favor selecione uma opção acima!"
                form={form}
                setForm={setForm}
                value={form.tipoPessoa}
                options={listTipoPessoa}
                placeholder="Escolha a opção"
                state="tipoPessoa"
                width={200}
              />
              <TextField
                label={res.personFisic ? "Nome Completo" : "Razão Social"}
                name="nomeRazaoSocial"
                placeholder={
                  res.personFisic
                    ? "Digite o nome social"
                    : "Digite a razão social"
                }
                width={350}
                disabled={!form.tipoPessoa}
              />
              {res.personFisic ? (
                <MaskInput
                  label="CPF"
                  mask={{
                    blocks: [3, 3, 3, 2],
                    delimiters: [".", ".", "-"],
                    numericOnly: true,
                  }}
                  error={`Por favor digite um CPF válido!`}
                  form={form}
                  value={form.cpfCnpj}
                  setForm={setForm}
                  invalid={errors.cpfCnpj}
                  state="cpfCnpj"
                  placeholder="Digite o CPF"
                  width={200}
                />
              ) : null}
              {!res.personFisic ? (
                <MaskInput
                  label="CNPJ"
                  mask={{
                    blocks: [2, 3, 3, 4, 2],
                    delimiters: [".", ".", "/", "-"],
                    numericOnly: true,
                  }}
                  error={`Por favor digite um CNPJ válido!`}
                  form={form}
                  value={form.cpfCnpj}
                  setForm={setForm}
                  invalid={errors.cpfCnpj}
                  state="cpfCnpj"
                  placeholder="Digite o CPNJ"
                  width={300}
                />
              ) : null}
            </FlexboxGrid>
          </div>
          <FlexboxGrid
            justify="end"
            style={{ marginTop: 30, marginBottom: 20 }}
          >
            {res.editing ? (
              <Button
                appearance="ghost"
                color="red"
                type="button"
                onClick={() => {
                  setForm(res.initialForm);
                  setAdditionalForm(res.initialAdditionalForm);
                  setFiles({});
                }}
              >
                Cancelar
              </Button>
            ) : null}

            <Button
              type="submit"
              appearance="primary"
              style={{ marginLeft: 16 }}
            >
              Salvar
            </Button>
          </FlexboxGrid>
        </div>
      </Form>
      <PersonNavGrid {...res} />
    </>
  );
};

export default DadosPessoaisForm;
