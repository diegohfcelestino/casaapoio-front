import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Form, InputPicker, FlexboxGrid, Button, Schema } from "rsuite";

import SelectInput from "../../../components/form/selectInput/SelectInput";
import TextInput from "../../../components/form/textInput/TextInput";

import InputModal from "../../../components/inputModal/InputModal";
import TextField from "../../../components/textField/TextField";
import ModalSelect from "../../../components/modalSelect/ModalSelect";

import { createDadosAdditional } from "../../../store/modules/person/action";
import {
  cboStore,
  naturalidadeStore,
  nacionalidadeStore,
  getEscolaridade,
  getEstadoCivil,
  handleLoadDadosAdditional,
} from "../PessoaServices";
import { gridDataSource as NumeroCalcadoStore } from "../../../store/modules/sizeShoe/action";
import { gridDataSource as TamanhoVestuarioStore } from "../../../store/modules/sizeClothing/action";
import { gridDataSource as ReligionStore } from "../../../store/modules/religion/action";

const DadosAdicionaisForm = ({
  editing,
  additionalForm,
  setAdditionalForm,
  initialAdditionalForm,
  initialForm,
  form,
  setForm,
  setFiles,
  setActiveTab,
  successMessage,
  errorMessage,
}) => {
  const dispatch = useDispatch();

  const { StringType } = Schema.Types;
  const model = Schema.Model({
    sexo: StringType().isRequired("Sexo é requirido"),
  });

  const listSexo = [
    {
      name: "Feminino",
      value: "F",
    },
    {
      name: "Masculino",
      value: "M",
    },
  ];

  const [errors, setErrors] = useState({});

  const [toggleProfissao, setToggleProfissao] = useState(false);
  const [toggleNaturalidade, setToggleNaturalidade] = useState(false);
  const [toggleNacionalidade, setToggleNacionalidade] = useState(false);
  const [toggleNumeroCalcado, setToggleNumeroCalcado] = useState(false);
  const [toggleTamanhoCamisa, setToggleTamanhoCamisa] = useState(false);
  const [toggleTamanhoCalca, setToggleTamanhoCalca] = useState(false);
  const [toggleReligiao, setToggleReligiao] = useState(false);

  const [listEscolaridade, setListEscolaridade] = useState([]);
  const [listEstadoCivil, setListEstadoCivil] = useState([]);

  useEffect(() => {
    getEscolaridade("", setListEscolaridade);
    getEstadoCivil("", setListEstadoCivil);
  }, []);

  useEffect(() => {
    if (form.idCaPessoa) {
      setErrors({});
    }
  }, [form]);

  const toggleModalNacionalidade = (event) => {
    setToggleNacionalidade(!toggleNacionalidade);
    setAdditionalForm({
      ...additionalForm,
      idNacionalidade: event?.idCrPais,
      descricaoNacionalidade: event?.nomepais,
    });
  };

  const toggleModalNaturalidade = (event) => {
    setToggleNaturalidade(!toggleNaturalidade);
    setAdditionalForm({
      ...additionalForm,
      idNaturalidade: event?.idCrCidades,
      descricaoNaturalidade: event?.rCodigoCidade,
    });
  };

  const toggleModalProfissao = (event) => {
    setToggleProfissao(!toggleProfissao);
    setAdditionalForm({
      ...additionalForm,
      idProfissao: event?.idFpCbo,
      descricaoProfissao: event?.descricao,
    });
  };

  const toggleModalNumeroCalcado = (event) => {
    setToggleNumeroCalcado(!toggleNumeroCalcado);
    setAdditionalForm({
      ...additionalForm,
      idNumeroCalcado: event?.idCaNumeracaoCalcado,
      descricaoNumeroCalcado: event?.numero,
    });
  };

  const toggleModalTamanhoCamisa = (event) => {
    setToggleTamanhoCamisa(!toggleTamanhoCamisa);
    setAdditionalForm({
      ...additionalForm,
      idTamanhoCamisa: event?.idCaTamanhoVestuario,
      descricaoTamanhoCamisa: event?.tamanho,
    });
  };

  const toggleModalTamanhoCalca = (event) => {
    setToggleTamanhoCalca(!toggleTamanhoCalca);
    setAdditionalForm({
      ...additionalForm,
      idTamanhoCalca: event?.idCaTamanhoVestuario,
      descricaoTamanhoCalca: event?.tamanho,
    });
  };

  const toggleModalReligiao = (event) => {
    setToggleReligiao(!toggleReligiao);
    setAdditionalForm({
      ...additionalForm,
      idReligiao: event?.idCaReligiao,
      descricaoReligiao: event?.nome,
    });
  };

  function handleChange(value) {
    setAdditionalForm({ ...additionalForm, ...value });
  }

  function handleSubmit(isValid) {
    if (isValid) {
      dispatch(
        createDadosAdditional(
          additionalForm,
          setAdditionalForm,
          successMessage,
          errorMessage,
          form.idCaPessoa
        )
      );
      handleLoadDadosAdditional(form.idCaPessoa)
        .then((res) => setAdditionalForm({ ...additionalForm, ...res[0] }))
        .catch((error) => console.log(error));
    }
  }

  return (
    <>
      <ModalSelect
        dataSource={nacionalidadeStore}
        colums={[
          {
            dataField: "nomepais",
            caption: "País",
            width: "100%",
          },
        ]}
        title={"Nacionalidade"}
        visible={toggleNacionalidade}
        toggleModal={toggleModalNacionalidade}
        width={"40%"}
      />
      <ModalSelect
        dataSource={naturalidadeStore}
        colums={[
          {
            dataField: "cidade",
            caption: "Cidade",
            width: "70%",
          },
          {
            dataField: "uf",
            caption: "UF",
            width: "10%",
          },
          {
            dataField: "codigo",
            caption: "Código IBGE",
            alignment: "rigth",
          },
        ]}
        title={"Naturalidade"}
        visible={toggleNaturalidade}
        toggleModal={toggleModalNaturalidade}
        width={"60%"}
      />
      <ModalSelect
        dataSource={cboStore}
        colums={[
          {
            dataField: "descricao",
            caption: "Profissão",
            width: "70%",
          },
          {
            dataField: "cbo",
            caption: "CBO",
          },
        ]}
        title={"Profissão"}
        visible={toggleProfissao}
        toggleModal={toggleModalProfissao}
        width={"50%"}
      />
      <ModalSelect
        dataSource={NumeroCalcadoStore}
        colums={[
          {
            dataField: "numero",
            caption: "Numeração",
            alignment: "rigth",
          },
        ]}
        title={"Número Calçado"}
        visible={toggleNumeroCalcado}
        toggleModal={toggleModalNumeroCalcado}
        width={"30%"}
      />
      <ModalSelect
        dataSource={TamanhoVestuarioStore(2)}
        colums={[
          {
            dataField: "tamanho",
            caption: "Tamanho",
          },
        ]}
        title={"Tamanho Camisa"}
        visible={toggleTamanhoCamisa}
        toggleModal={toggleModalTamanhoCamisa}
        width={"30%"}
      />
      <ModalSelect
        dataSource={TamanhoVestuarioStore(1)}
        colums={[
          {
            dataField: "tamanho",
            caption: "Tamanho",
          },
        ]}
        title={"Tamanho Calça"}
        visible={toggleTamanhoCalca}
        toggleModal={toggleModalTamanhoCalca}
        width={"30%"}
      />
      <ModalSelect
        dataSource={ReligionStore}
        colums={[
          {
            dataField: "nome",
            caption: "Religião",
          },
        ]}
        title={"Religião"}
        visible={toggleReligiao}
        toggleModal={toggleModalReligiao}
        width={"30%"}
      />
      <Form
        model={model}
        formValue={additionalForm}
        formError={errors}
        onChange={handleChange}
        onSubmit={handleSubmit}
        onCheck={setErrors}
        fluid
        style={{ marginTop: 20, margin: 20 }}
      >
        <FlexboxGrid>
          <div style={{ margin: 8 }}>
            <label className="rs-control-label" for="nascimento">
              Nascimento
            </label>
            <input
              id="nascimento"
              className="rs-input"
              type="date"
              onChange={(e) => {
                if (e.target.value) {
                  setAdditionalForm({
                    ...additionalForm,
                    nascimento: new Date(`${e.target.value}T03:00:00Z`),
                  });
                }
              }}
              value={
                additionalForm.nascimento
                  ? new Date(additionalForm.nascimento)
                      .toISOString()
                      .slice(0, 10)
                  : null
              }
              max={new Date().toISOString().slice(0, 10)}
              style={{ width: 150, maxHeight: 36 }}
            />
          </div>
          <SelectInput
            label="Sexo"
            error="Por favor selecione uma opção acima!"
            form={additionalForm}
            setForm={setAdditionalForm}
            value={additionalForm.sexo}
            options={listSexo}
            placeholder="Escolha uma opção"
            state="sexo"
            width={150}
            invalid={errors.sexo}
          />
          <InputModal
            label="Nacionalidade"
            onClick={toggleModalNacionalidade}
            descricao={additionalForm?.descricaoNacionalidade}
            placeholder="Clique para selecionar a nacionalidade"
            readOnly={additionalForm?.trava}
            width={200}
          />
          <InputModal
            label="Naturalidade"
            onClick={toggleModalNaturalidade}
            descricao={additionalForm?.descricaoNaturalidade}
            placeholder="Clique para selecionar a naturalidade"
            readOnly={additionalForm?.trava}
            width={350}
          />
          <TextField
            name="idEstadoCivil"
            label="Estado Civil"
            accepter={InputPicker}
            labelKey="descricao"
            valueKey="idFpEstadoCivil"
            readOnly={additionalForm?.trava}
            data={listEstadoCivil}
            width={200}
          />
          <TextField
            name="idEscolaridade"
            label="Escolaridade"
            accepter={InputPicker}
            labelKey="descricao"
            valueKey="idFpGrauInstrucao"
            readOnly={additionalForm?.trava}
            data={listEscolaridade}
            width={450}
          />
          <InputModal
            label="Profissão"
            onClick={toggleModalProfissao}
            descricao={additionalForm?.descricaoProfissao}
            placeholder="Clique para selecionar a profissão"
            readOnly={additionalForm?.trava}
            width={550}
          />
          <TextInput
            label="Formação"
            error="Por favor digite uma formação válido!"
            form={additionalForm}
            tabIndex="16"
            value={additionalForm.formacao}
            setForm={setAdditionalForm}
            //invalid={invalid.invalidFormacao && form.formacaoBlur}
            state="formacao"
            stateBlur="formacaoBlur"
            placeholder="Escreva o nome da formação"
            width={350}
          />
          <InputModal
            label="Tamanho Camisa"
            onClick={toggleModalTamanhoCamisa}
            descricao={additionalForm?.descricaoTamanhoCamisa}
            placeholder="Clique para selecionar o tamanho"
            readOnly={additionalForm?.trava}
            width={120}
          />
          <InputModal
            label="Tamanho Calça"
            onClick={toggleModalTamanhoCalca}
            descricao={additionalForm?.descricaoTamanhoCalca}
            placeholder="Clique para selecionar o tamanho"
            readOnly={additionalForm?.trava}
            width={120}
          />
          <InputModal
            label="N° Calçado"
            onClick={toggleModalNumeroCalcado}
            descricao={additionalForm?.descricaoNumeroCalcado}
            placeholder="Clique para selecionar a numeração"
            readOnly={additionalForm?.trava}
            width={100}
          />
          <InputModal
            label="Religião"
            onClick={toggleModalReligiao}
            descricao={additionalForm?.descricaoReligiao}
            placeholder="Clique para selecionar a religião"
            readOnly={additionalForm?.trava}
            width={300}
          />
        </FlexboxGrid>

        <FlexboxGrid justify="end" style={{ marginTop: 30 }}>
          {editing ? (
            <Button
              appearance="ghost"
              color="red"
              type="button"
              onClick={() => {
                setAdditionalForm(initialAdditionalForm);
                setForm(initialForm);
                setFiles({});
                setActiveTab("primaryForm");
              }}
            >
              Cancelar
            </Button>
          ) : null}

          <Button type="submit" appearance="primary" style={{ marginLeft: 16 }}>
            Salvar Adicional
          </Button>
        </FlexboxGrid>
      </Form>
    </>
  );
};

export default DadosAdicionaisForm;
