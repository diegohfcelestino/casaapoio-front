import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import { cpf } from "cpf-cnpj-validator";
import qs from "qs";

export function createAcompanhamento(
  form,
  setForm,
  successMessage,
  errorMessage
) {
  if (cpf.isValid(form?.cpf)) {
    const user = JSON.parse(sessionStorage.getItem(Auth));
    const now = new Date();
    api
      .post("CaAcompanhamentoCaso", {
        empresa: user.empresa,
        idCaAcompanhamentoCaso: 0,
        excluido: "F",
        ...form,
        usuario: user.idUsuario,
        dataInclusao: now.toDateString(),
      })
      .then((res) => {
        successMessage("Cadastro feito com sucesso");
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao cadastrar", err));
      });
  } else {
    errorMessage("Informe um CPF válido.");
  }
}

export function alterAcompanhamento(form, successMessage, errorMessage) {
  if (cpf.isValid(form?.cpf)) {
    const user = JSON.parse(sessionStorage.getItem(Auth));
    const now = new Date();
    if (form?.idCaAcompanhamentoCaso) {
      api
        .put("CaAcompanhamentoCaso", {
          empresa: user.empresa,
          excluido: "F",
          ...form,
          usuario: user.idUsuario,
          dataInclusao: now.toDateString(),
        })
        .then((res) => {
          successMessage("Alteração feita com sucesso");
        })
        .catch((err) => {
          errorMessage(handleErrorForm("Erro ao alterar", err));
        });
    } else {
      errorMessage(
        "Acompanhamento não é válido, por favor faça a busca novamente."
      );
    }
  } else {
    errorMessage("Informe um CPF válido.");
  }
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaAcompanhamentoCaso/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`CaAcompanhamentoCaso/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaAcompanhamentoCaso",
    load: handleLoad,
    remove: handleRemove,
  }),
};
