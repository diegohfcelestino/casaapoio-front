import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import { cpf } from "cpf-cnpj-validator";
import qs from "qs";

export function createEntrevistaSocial(
  form,
  descricao,
  setForm,
  successMessage,
  errorMessage
) {
  if (cpf.isValid(form?.cpf)) {
    const user = JSON.parse(sessionStorage.getItem(Auth));
    const now = new Date();
    api
      .post("CaEntrevistaSocial", {
        empresa: user.empresa,
        idCaEntrevistaSocial: 0,
        excluido: "F",
        idCaHospedagem: null,
        ...form,
        descricao,
        usuario: user.idUsuario,
        dataInclusao: now.toDateString(),
      })
      .then((res) => {
        successMessage("Cadastro feito com sucesso");
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao cadastrar", err));
      });
  } else {
    errorMessage("Informe um CPF válido.");
  }
}

export function alterEntrevistaSocial(
  form,
  descricao,
  successMessage,
  errorMessage
) {
  if (cpf.isValid(form?.cpf)) {
    const user = JSON.parse(sessionStorage.getItem(Auth));
    const now = new Date();
    if (form?.idCaEntrevistaSocial) {
      api
        .put("CaEntrevistaSocial", {
          empresa: user.empresa,
          excluido: "F",
          idCaHospedagem: null,
          descricao,
          ...form,
          usuario: user.idUsuario,
          dataInclusao: now.toDateString(),
        })
        .then((res) => {
          successMessage("Alteração feita com sucesso");
        })
        .catch((err) => {
          errorMessage(handleErrorForm("Erro ao alterar", err));
        });
    } else {
      errorMessage(
        "EntrevistaSocial não é válido, por favor faça a busca novamente."
      );
    }
  } else {
    errorMessage("Informe um CPF válido.");
  }
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaEntrevistaSocial/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`CaEntrevistaSocial/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaEntrevistaSocial",
    load: handleLoad,
    remove: handleRemove,
  }),
};
