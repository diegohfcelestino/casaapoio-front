import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import apiXHR from "../../../services/apiXHR";
import { Auth } from "../../../config/storage";
import {
  handleError,
  handleErrorForm,
  handleParams,
} from "../../../services/helper";
import store from "../../Store";
import removerAcentos from "../../../services/removerAcentos";

export function selectDetail(selectedDetail) {
  return {
    type: "SELECTED_DETAIL",
    selectedDetail,
  };
}

export function getFoto(idPessoa, setFiles) {
  const params = {
    id: idPessoa,
  };
  const url = `CaPessoa/DownloadFotoBase64?${qs.stringify(params)}`;

  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then((response) => resolve(setFiles(response.data)))
      .catch((error) => {
        setFiles({});
        reject(error);
      });
  });
}

export function createPerson(
  form,
  setForm,
  successMessage,
  errorMessage,
  additionalForm,
  setAdditionalForm,
  files,
  setFiles
) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const method = form.idCaPessoa ? "put" : "post";
  const action = form.idCaPessoa ? "Alteração feita" : "Cadastro feito";
  const auxDataObj = {
    ...form,
    empresa: auth.empresa,
    dataInclusao: form.dataInclusao ?? new Date(),
    usuario: auth.nomeUsuario,
    excluido: "F",
  };
  var data = new FormData();
  data.append("caPessoaVo", JSON.stringify(auxDataObj));
  data.append("foto", files.data);
  const url = "/CaPessoa";
  return () =>
    apiXHR({ method, url, data })
      .then((response) => {
        const { data } = response;
        setForm({ ...data });
        setAdditionalForm({ ...additionalForm, idCaPessoa: data.idCaPessoa });
        successMessage(`${action} com sucesso`);
        getFoto(data.idCaPessoa, setFiles);
      })
      .catch((err) => {
        errorMessage(handleErrorForm(`Erro no ${action.toLowerCase()}`, err));
      });
}

export function createDadosAdditional(
  additionalForm,
  setAdditionalForm,
  successMessage,
  errorMessage,
  idPessoa
) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const method = additionalForm.idCaPessoaDadosAdic ? "put" : "post";
  const action = additionalForm.idCaPessoaDadosAdic
    ? "Alteração feita"
    : "Cadastro feito";
  const data = {
    ...additionalForm,
    empresa: auth.empresa,
    dataInclusao: new Date(),
    usuario: auth.nomeUsuario,
    excluido: "F",
    idCaPessoa: idPessoa,
  };

  const url = "/CaPessoaDadosAdic";
  return () => {
    api({ method, url, data })
      .then((response) => {
        const { data } = response;
        setAdditionalForm({ ...data });
        successMessage(`${action} com sucesso`);
      })
      .catch((err) => {
        errorMessage(handleErrorForm(`Erro no ${action.toLowerCase()}`, err));
      });
  };
}

function handleLoadTypeGeneric(loadOptions, url) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    skip: 0,
    take: 1000,
  };
  const urlComplete = `${url}?${qs.stringify(params)}`;

  return api
    .get(urlComplete)
    .then((res) => {
      const { data, totalCount } = res.data;

      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoad(loadOptions, url, id) {
  const params = {
    idPessoa: id,
  };
  const newParams = handleParams(params, loadOptions);
  const urlFormated = `${url}?${qs.stringify(newParams)}`;

  return api
    .get(urlFormated)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadCity(loadOptions, url, sigla) {
  const params = {
    uf: sigla,
  };
  const newParams = handleParams(params, loadOptions);
  const urlFormated = `${url}?${qs.stringify(newParams)}`;

  return api
    .get(urlFormated)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleCepInfo({ localidade, uf }) {
  const params = {
    cidade: removerAcentos(localidade),
    uf: uf,
    skip: 0,
    take: 1,
  };
  const url = `CrCidades/GetCrCidadeAutoComplete?${qs.stringify(params)}`;

  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
}

function handleLoadPerson(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    nome: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaPessoa/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemovePerson({ idCaPessoa }) {
  return api
    .delete(`/CaPessoa/Id/${idCaPessoa}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export function handlePost(data, url) {
  return api
    .post(url, data)
    .then((res) => res)
    .catch((err) => handleError(`Erro ao incluir`, err));
}

export function handlePut(param, url, name, key) {
  const record = store.getState().Person.selectedDetail;
  const data = {
    ...param,
    ...record,
    [name]: key,
  };
  return api
    .put(url, data)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export function handleDelete(id, url) {
  return api
    .delete(`${url}${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleLoadByKey(key, name, url) {
  const params = {
    [name]: key,
  };
  const newUrl = `${url}${qs.stringify(params)}`;
  return api
    .get(newUrl)
    .then(({ data }) => {
      return [{ ...data }];
    })
    .catch((error) => handleError(error.response));
}

export function gridDataSourceContact(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaContato",
      load: (loadOptions) => {
        const url = "/CaPessoaContato/GetPessoaContatoByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaContato";
        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaContato";
        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          excluido: "F",
          idCaTipoContato: values.idCaTipoContato,
          ...values,
          ...key,
        };
        return handlePut(record, url, "idCaPessoaContato", key);
      },
      remove: (values) => {
        const url = "/CaPessoaContato/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export const gridDataSourceContactType = {
  store: new CustomStore({
    key: "idCaTipoContato",
    byKey: (key, loadOptions) =>
      handleLoadByKey(
        key,
        "idCaTipoContato",
        "CaTipoContato/GetCaTipoContatoById?"
      ),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CaTipoContato/GetPorEmpresa"),
  }),
};

export function gridDataSourceFamilyComposition(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaCompoFamiliar",
      load: (loadOptions) => {
        const url = "/CaPessoaCompoFamiliar/GetCompoFamiliarByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaCompoFamiliar";
        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaCompoFamiliar";
        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          excluido: "F",
          ...values,
          ...key,
        };
        return handlePut(record, url, "idCaPessoaCompoFamiliar", key);
      },
      remove: (values) => {
        const url = "/CaPessoaCompoFamiliar/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export function gridDataSourceEmail(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaEmail",
      load: (loadOptions) => {
        const url = "/CaPessoaEmail/GetPessoaEmailByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaEmail";
        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
          principal: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaEmail";
        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          excluido: "F",
          ...values,
          ...key,
        };
        return handlePut(record, url, "idCaPessoaEmail", key);
      },
      remove: (values) => {
        const url = "/CaPessoaEmail/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export const gridDataSourceEmailType = {
  store: new CustomStore({
    key: "idCaTipoEmail",
    byKey: (key, loadOptions) =>
      handleLoadByKey(key, "idCaTipoEmail", "CaTipoEmail/GetCaTipoEmailById?"),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CaTipoEmail/GetPorEmpresa"),
  }),
};

export function gridDataSourceAddress(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaEndereco",
      load: (loadOptions) => {
        const url = "/CaPessoaEndereco/GetPessoaEnderecoByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaEndereco";
        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
          principal: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaEndereco";
        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          excluido: "F",
          ...values,
        };
        return handlePut(record, url, "idCaPessoaEndereco", key);
      },
      remove: (values) => {
        const url = "/CaPessoaEndereco/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export const gridDataSourceAddressType = {
  store: new CustomStore({
    key: "idCaTipoEndereco",
    byKey: (key, loadOptions) =>
      handleLoadByKey(
        key,
        "idCaTipoEndereco",
        "CaTipoEndereco/GetCaTipoEndereco?"
      ),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CaTipoEndereco/GetPorEmpresa"),
  }),
};

export function gridDataSourceDocument(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaDocumento",

      load: (loadOptions) => {
        const url = "/CaPessoaDocumento/GetPessoaDocumentoByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaDocumento";
        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaDocumento";
        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          excluido: "F",
          ...values,
          ...key,
        };
        return handlePut(record, url, "idCaPessoaDocumento", key);
      },
      remove: (values) => {
        const url = "/CaPessoaDocumento/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export const gridDataSourceDocumentType = {
  store: new CustomStore({
    key: "idCaTipoDocumento",
    byKey: (key, loadOptions) =>
      handleLoadByKey(
        key,
        "idCaTipoDocumento",
        "CaTipoDocumento/GetCaTipoDocumentoById?"
      ),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CaTipoDocumento/GetPorEmpresa"),
  }),
};

export function gridDataSourcePhone(id) {
  return {
    store: new CustomStore({
      key: "idCaPessoaTelefone",
      load: (loadOptions) => {
        const url = "/CaPessoaTelefone/GetPessoaTelefoneByPessoa";
        return handleLoad(loadOptions, url, id);
      },
      insert: (values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaTelefone";

        const record = {
          ...values,
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
          principal: "F",
        };
        return handlePost(record, url);
      },
      update: (key, values) => {
        const auth = JSON.parse(sessionStorage.getItem(Auth));
        const url = "/CaPessoaTelefone";

        const record = {
          idCaPessoa: id,
          empresa: auth.empresa,
          usuario: auth.nomeUsuario,
          excluido: "F",
          ...values,
          ...key,
        };
        return handlePut(record, url, "idCaPessoaTelefone", key);
      },
      remove: (values) => {
        const url = "/CaPessoaTelefone/Id/";
        return handleDelete(values, url);
      },
    }),
  };
}

export function handleLoadNecessidadeEspecial(loadOptions, id) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idPessoaEmpresa: auth.empresa,
    idPessoa: id,
    descricaoNecessidade: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaPessoaNecesEspecial/GetPessoaNecesEspecial?${qs.stringify(
    newParams
  )}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}
export function handleLoadPessoaClassificacao(loadOptions, id) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const params = {
    empresa: auth.empresa,
    idCaPessoa: id,
    descricaoClassificacaoPessoa: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaPessoaClassificacao/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadNecessidadePrecaucao(loadOptions, id) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idPessoaEmpresa: auth.empresa,
    idPessoa: id,
    descricaoNecessidade: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaPessoaNecePrecaucao/GetCaPessoaNecePrecaucao?${qs.stringify(
    newParams
  )}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export const gridDataSourcePhoneType = {
  store: new CustomStore({
    key: "idCaTipoTelefone",
    byKey: (key, loadOptions) =>
      handleLoadByKey(
        key,
        "idCaTipoTelefone",
        "CaTipoTelefone/GetCaTipoTelefoneById?"
      ),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CaTipoTelefone/GetPorEmpresa"),
  }),
};

export function gridDataSourceCidade(sigla) {
  return {
    store: new CustomStore({
      key: "idCrCidades",
      byKey: (loadOptions) =>
        handleLoadCity(
          loadOptions,
          "/CrCidades/GetCrCidadeAutoComplete",
          sigla
        ),
      load: (loadOptions) =>
        handleLoadCity(
          loadOptions,
          "/CrCidades/GetCrCidadeAutoComplete",
          sigla
        ),
    }),
  };
}
export const gridDataSourceUf = {
  store: new CustomStore({
    key: "idCrUf",
    byKey: (loadOptions) =>
      handleLoad(loadOptions, "/CrUf/GetCrUfAutoComplete"),
    load: (loadOptions) => handleLoad(loadOptions, "/CrUf/GetCrUfAutoComplete"),
  }),
};
export const gridDataSourceTipoLogradouro = {
  store: new CustomStore({
    key: "idFpTipoLogradouro",
    byKey: (loadOptions) =>
      handleLoad(
        loadOptions,
        "/FpTipoLogradouro/GetFpTipoLogradouroAutoComplete"
      ),
    load: (loadOptions) =>
      handleLoad(
        loadOptions,
        "/FpTipoLogradouro/GetFpTipoLogradouroAutoComplete"
      ),
  }),
};

export function gridDataSourceNecessidadeEspecial(id) {
  return {
    store: new CustomStore({
      key: ["id"],
      load: (loadOptions) => {
        return handleLoadNecessidadeEspecial(loadOptions, id);
      },
    }),
  };
}

export function gridDataSourcePessoaClassificacao(id) {
  return {
    store: new CustomStore({
      key: ["id"],
      load: (loadOptions) => {
        return handleLoadPessoaClassificacao(loadOptions, id);
      },
    }),
  };
}

export function gridDataSourceNecessidadePrecaucao(id) {
  return {
    store: new CustomStore({
      key: ["id"],
      load: (loadOptions) => {
        return handleLoadNecessidadePrecaucao(loadOptions, id);
      },
    }),
  };
}

export const gridDataSourcePerson = {
  store: new CustomStore({
    key: ["idCaPessoa"],
    load: handleLoadPerson,
    remove: handleRemovePerson,
  }),
};
