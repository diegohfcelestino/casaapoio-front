export default (state = {}, action) => {
  switch (action.type) {
    case "ADD_ID_PERSON":
      return { ...state, idPessoa: action.idPessoa };
    case "SELECTED_DETAIL":
      return { ...state, selectedDetail: action.selectedDetail };
    default:
      return state;
  }
};
