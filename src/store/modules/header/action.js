export function tabsHeader(tabs) {
  return (dispatch) => {
    dispatch({
      type: "TABS_HEADER",
      tabs,
    });
  }
}
