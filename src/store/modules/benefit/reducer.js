const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_BENEFIT":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_BENEFIT":
      return {
        ...state,
        selectedBenefit: action.selectedBenefit,
      };
    case "UPDATE_BENEFIT":
      return initialState;
    case "DELETE_BENEFIT":
      return initialState;
    case "CLEAN_DATA_BENEFIT":
      return initialState;
    default:
      return state;
  }
};
