import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleError,
  filterNonNull,
  handleErrorForm,
  handleParams,
} from "../../../services/helper";

export function addidItem(id) {
  return {
    type: "ADD_ID_ITEM",
    idItem: id,
  };
}

export function selectDetail(selectedDetail) {
  return {
    type: "SELECTED_DETAIL",
    selectedDetail,
  };
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    cpf:null,
    nome: null,
    descricaoHospital:null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaSolicitaHospedagem/GetHospedagemAprovada?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

// function handleRemove(id) {
//   return api
//     .delete(`/CaSolicitaHospedagem/Id/${id}`)
//     .then((res) => res)
//     .catch((err) => handleError("Erro ao remover", err));
// }

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaSolicitaHospedagem",
    load: handleLoad,
    //remove: handleRemove,
  }),
}