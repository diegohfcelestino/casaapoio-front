import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { postAndPut } from "./object";
import {
  handleError,
  filterNonNull,
  handleErrorForm,
  handleParams,
} from "../../../services/helper";
import removerAcentos from "../../../services/removerAcentos";
import store from "../../Store";

export function addidItem(id) {
  return {
    type: "ADD_ID_ITEM",
    idItem: id,
  };
}

export function selectDetail(selectedDetail) {
  return {
    type: "SELECTED_DETAIL",
    selectedDetail,
  };
}

export function putHospedagem(
  form,
  setForm,
  successMessage,
  errorMessage
) {
const auth = JSON.parse(sessionStorage.getItem(Auth));
  return (dispatch) => {
    const data = {
      ...form,
      dataInclusao: new Date(),
      usuario: auth.nomeUsuario,
      excluido: "F",
    };
    const url = "/CaHospedagem";

    api.put(url, data)
      .then((response) => {
        const { data } = response;
        // dispatch(addIdEdit(data.id));
        setForm({
          ...data
        });
        successMessage(`Alteração feita com sucesso`);
      })
      .catch((err) => {
        errorMessage(handleErrorForm(`Erro ao alterar hospedagem`, err));
      });
  };
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaHospedagem/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`/CaHospedagem/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaHospedagem",
    load: handleLoad,
    remove: handleRemove,
  }),
}

export function handleFormData(setForm, newForm, handleInfoValues) {
  const url = `CaComposicaoAcomodacao/GetDescricaoComposicao?idCaComposicaoAcomodacao=${newForm.idCaComposicaoAcomodacao}`;
  return (dispatch) => {
    return api
      .get(url)
      .then(({ data }) => {
        setForm({
          ...newForm,
          idCaSolitacaoHospedagem:newForm.idSolitacaoHospedagem,
          descricaoAcomodacao: data ? data[0].descricaoAcomodacao : "",
          info: handleInfoValues(newForm),
        });
        const {idCaHospedagem} = newForm;
        dispatch(addidItem(idCaHospedagem));
      })
      .catch((err) => handleErrorForm("Erro ao carregar", err));
  };
}