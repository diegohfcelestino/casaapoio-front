import { Auth } from "../../../config/storage";

export function postAndPut(form) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  return {
    idCaHospedagem: form.idCaHospedagem,
    empresa: auth.empresa,
    cpf: form.cpf,
    idPessoaHospital: form.idPessoaHospital,
    vacinacaoAtualizada: form.vacinacaoAtualizada,
    fumante: form.fumante,
    precaucao: form.precaucao,
    tfd: form.tfd,
    sus: form.sus,
    assistenteSocial: form.assistenteSocial,
    telefoneAssisSocialPrincipal: form.telefoneAssisSocialPrincipal,
    telefoneAssisSocialOpcional: form.telefoneAssisSocialOpcional,
    emailAssistSocial: form.emailAssistSocial,
    hipoteseDiagnostica: form.hipoteseDiagnostica,
    idCid: form.idCid,
    observacao: form.observacao,
    idCaComposicaoAcomodacao: form.idCaComposicaoAcomodacao,
    idSolicitaHospedagem: form.idSolicitaHospedagem,
    idCaSolicitaHospedagem: form.idCaSolicitaHospedagem,
    previsaoChegada: form.previsaoChegada,
    entrada:form.entrada,
    alta:form.alta,
    idSituacao: form.idSituacao,
  };
//   alta: "2021-03-16T15:49:31.436"
// assistenteSocial: "string"
// cpf: "11111111111"
// dataInclusao: "2021-03-16T13:07:17.3511"
// emailAssistSocial: "string"
// empresa: 11
// entrada: "2021-03-16T15:49:31.436"
// excluido: "F"
// fumante: "F"
// hipoteseDiagnostica: "string"
// idCaComposicaoAcomodacao: 28
// idCaHospedagem: 1
// idCid: null
// idPessoaHospital: 107
// idSituacao: 1
// idSolitacaoHospedagem: 3
// idTamanhoFralda: null
// observacao: "string"
// precaucao: "F"
// previsaoChegada: "2021-03-16T15:49:31.436"
// sus: "F"
// telefoneAssisSocialOpcional: "string"
// telefoneAssisSocialPrincipal: "string"
// tfd: "F"
// usuario: "LUCAS"
// vacinacaoAtualizada: "T"
}
