const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_TYPE_ADDRESS":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_TYPE_ADDRESS":
      return {
        ...state,
        selectedTypeAddress: action.selectedTypeAddress,
      };
    case "UPDATE_TYPE_ADDRESS":
      return initialState;
    case "DELETE_TYPE_ADDRESS":
      return initialState;
    case "CLEAN_DATA_TYPE_ADDRESS":
      return initialState;
    default:
      return state;
  }
};
