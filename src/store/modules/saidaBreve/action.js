import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import qs from "qs";

export function createSaidaBreve(form, setForm, successMessage, errorMessage) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  api
    .post("CaSaidaBreve", {
      empresa: user.empresa,
      idCaSaidaBreve: 0,
      excluido: "F",
      ...form,
      usuario: user.nomeUsuario,
      dataInclusao: now.toDateString(),
    })
    .then((res) => {
      successMessage("Cadastro feito com sucesso");
    })
    .catch((err) => {
      errorMessage(handleErrorForm("Erro ao cadastrar", err));
    });
}

export function alterSaidaBreve(form, successMessage, errorMessage) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  if (form?.idCaSaidaBreve) {
    api
      .put("CaSaidaBreve", {
        empresa: user.empresa,
        excluido: "F",
        ...form,
        usuario: user.nomeUsuario,
        dataInclusao: now.toDateString(),
      })
      .then((res) => {
        successMessage("Alteração feita com sucesso");
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao alterar", err));
      });
  } else {
    errorMessage("SaidaBreve não é válido, por favor faça a busca novamente.");
  }
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CeProduto/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`CaSaidaBreve/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCeProduto",
    load: handleLoad,
    remove: handleRemove,
  }),
};

export function getPaciente(idCaHospedagem) {
  return (dispatch) => {
    dispatch({
      type: "GET_PACIENTE",
      idCaHospedagem,
    });
  };
}

