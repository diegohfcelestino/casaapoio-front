import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import qs from "qs";

export function createEncaminhamentoParceiroMes(
  form,
  setForm,
  successMessage,
  errorMessage
) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  api
    .post("CaParceiroEncaminMes", {
      empresa: user.empresa,
      idCaParceiroEncaminMes: 0,
      excluido: "F",
      ...form,
      usuario: user.idUsuario,
      dataInclusao: now.toDateString(),
    })
    .then((res) => {
      successMessage("Cadastro feito com sucesso");
    })
    .catch((err) => {
      errorMessage(handleErrorForm("Erro ao cadastrar", err));
    });
}

export function alterEncaminhamentoParceiroMes(
  form,
  successMessage,
  errorMessage
) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  if (form?.idCaParceiroEncaminMes) {
    api
      .put("CaParceiroEncaminMes", {
        empresa: user.empresa,
        excluido: "F",
        ...form,
        usuario: user.idUsuario,
        dataInclusao: now.toDateString(),
      })
      .then((res) => {
        successMessage("Alteração feita com sucesso");
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao alterar", err));
      });
  } else {
    errorMessage(
      "EncaminhamentoParceiroMes não é válido, por favor faça a busca novamente."
    );
  }
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaParceiroEncaminMes/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`CaParceiroEncaminMes/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaParceiroEncaminMes",
    load: handleLoad,
    remove: handleRemove,
  }),
};
