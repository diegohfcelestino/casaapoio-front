import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    id: 0,
    ...values,
    idTipoVestuario: values.idTipoVestuario ?? null,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("CaTamanhoVestuario", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions, id) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
  };
  const auxObj = {
    idEmpresa: auth.empresa,
    idTipoVestuario: id,
  };
  const newParams = handleParams(id ? auxObj : params, loadOptions);
  const url = `CaTamanhoVestuario/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCaTamanhoVestuario }) {
  return api
    .delete(`CaTamanhoVestuario/Id/${idCaTamanhoVestuario}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...key,
    ...values,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .put("CaTamanhoVestuario", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

function handleLoadClothingType() {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
  };

  const urlComplete = `CaTipoVestuario/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(urlComplete)
    .then((res) => {
      const { data } = res.data;
      return {
        data,
        totalCount: 0,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleLoadByKey(key, url) {
  const params = {
    idCaTipoVestuario: key,
  };
  const newUrl = `${url}${qs.stringify(params)}`;
  return api
    .get(newUrl)
    .then(({ data }) => {
      return [{ ...data }];
    })
    .catch((error) => handleError(error.response));
}

export const gridDataSourceClothingType = {
  store: new CustomStore({
    key: "idCaTipoVestuario",
    byKey: (key, loadOptions) =>
      handleLoadByKey(key, "CaTipoVestuario/GetCaTipoVestuarioById?"),
    load: (loadOptions) => handleLoadClothingType(),
  }),
};

export function gridDataSource(id = null) {
  return {
    store: new CustomStore({
      key: ["idCaTamanhoVestuario", "dataInclusao"],
      load: (loadOptions) => handleLoad(loadOptions, id),
      insert: handleInsert,
      update: handleUpdate,
      remove: handleRemove,
    }),
  };
}
