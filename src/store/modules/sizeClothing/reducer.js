const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_SIZE_CLOTHING":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_SIZE_CLOTHING":
      return {
        ...state,
        selectedSizeClothing: action.selectedSizeClothing,
      };
    case "UPDATE_SIZE_CLOTHING":
      return initialState;
    case "DELETE_SIZE_CLOTHING":
      return initialState;
    case "CLEAN_DATA_SIZE_CLOTHING":
      return initialState;
    default:
      return state;
  }
};
