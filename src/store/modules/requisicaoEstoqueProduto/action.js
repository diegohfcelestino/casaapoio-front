import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleError,
  handleErrorForm,
  handleParams,
} from "../../../services/helper";

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeRequisicaoProduto/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`/CeRequisicaoProduto/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCeRequisicaoProduto",
    load: handleLoad,
    remove: handleRemove,
  }),
};
