import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCaCentroCusto: 0,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("CeCentroCusto", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    codigo: '',
    descricao: ""
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeCentroCusto/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}


function handleLoadSelect(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    codigo: '',
    descricao: "",
    skip: 0,
    take: 1000
  };
  const url = `/CeCentroCusto/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeCentroCusto }) {
  return api
    .delete(`/CeCentroCusto/Id/${idCeCentroCusto}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const record = {
    ...key,
    ...values,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .put("/CeCentroCusto", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSourceSelect = {
  store: new CustomStore({
    key: ["idCeCentroCusto"],
    load: handleLoadSelect,
  }),
};

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCeCentroCusto"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
