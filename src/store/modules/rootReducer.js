/* eslint-disable no-undef */
import { combineReducers } from "redux";

import SolicitacaoHospedagem from "./solicitacaoHospedagem/reducer";
import Ala from "./ala/reducer";
import Leito from "./leito/reducer";
import Header from "./header/reducer";
import Home from "./home/reducer";
import ComposicaoAcomodacao from "./composicaoAcomodacao/reducer";
import Login from "./login/reducer";
import Sidebar from "./sidebar/reducer";
import Benefit from "./benefit/reducer";
import PrecautionNeed from "./precautionNeed/reducer";
import PrecautionNeedType from "./precautionNeedType/reducer";
import Religion from "./religion/reducer";
import Quarto from "./quarto/reducer";
import SizeClothing from "./sizeClothing/reducer";
import SizeShoe from "./sizeShoe/reducer";
import SpecialNeed from "./specialNeed/reducer";
import SubLeito from "./subLeito/reducer";
import Person from "./person/reducer";
import TreatmentType from "./treatmentType/reducer";
import TypeAddress from "./typeAddress/reducer";
import TypeContact from "./typeContact/reducer";
import TypeDocument from "./typeDocument/reducer";
import TypeEmail from "./typeEmail/reducer";
import TypePhone from "./typePhone/reducer";
import Hospedagem from "./hospedagem/reducer";
import SaidaBreve from "./saidaBreve/reducer";
import EntradaHospedagem from "./entradaHospedagem/reducer";
import CentroCusto from "./centroCusto/reducer";
import OperacaoEstoque from "./operacaoEstoque/reducer";
import GrupoProduto from "./grupoProduto/reducer";
import UnidadeMedida from "./unidadeMedida/reducer";
import SubgrupoProduto from "./subgrupoProduto/reducer";
import SituacaoCadastral from "./situacaoCadastral/reducer";
import Produto from "./produto/reducer";

const reducers = combineReducers({
  Ala,
  SolicitacaoHospedagem,
  Leito,
  Header,
  Home,
  ComposicaoAcomodacao,
  Login,
  Sidebar,
  Benefit,
  PrecautionNeed,
  PrecautionNeedType,
  Religion,
  Quarto,
  SizeClothing,
  SizeShoe,
  SpecialNeed,
  SubLeito,
  TreatmentType,
  TypeAddress,
  TypeContact,
  TypeDocument,
  TypeEmail,
  TypePhone,
  Person,
  Hospedagem,
  SaidaBreve,
  EntradaHospedagem,
  OperacaoEstoque,
  GrupoProduto,
  UnidadeMedida,
  CentroCusto,
  SituacaoCadastral,
  SubgrupoProduto,
  Produto,
});

// eslint-disable-next-line import/no-anonymous-default-export
export default (state, action) => {
  if (action.type === "LOGOUT") {
    state = undefined;
  }

  return reducers(state, action);
};
