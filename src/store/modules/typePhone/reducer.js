const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_TYPE_PHONE":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_TYPE_PHONE":
      return {
        ...state,
        selectedTypePhone: action.selectedTypePhone,
      };
    case "UPDATE_TYPE_PHONE":
      return initialState;
    case "DELETE_TYPE_PHONE":
      return initialState;
    case "CLEAN_DATA_TYPE_PHONE":
      return initialState;
    default:
      return state;
  }
};
