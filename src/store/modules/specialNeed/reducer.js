const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_SPECIAL_NEED":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_SPECIAL_NEED":
      return {
        ...state,
        selectedSpecialNeed: action.selectedSpecialNeed,
      };
    case "UPDATE_SPECIAL_NEED":
      return initialState;
    case "DELETE_SPECIAL_NEED":
      return initialState;
    case "CLEAN_DATA_SPECIAL_NEED":
      return initialState;
    default:
      return state;
  }
};
