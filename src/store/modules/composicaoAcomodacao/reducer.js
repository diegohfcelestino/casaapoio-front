// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SELECT_ITEM":
      return {
        selected: action.selected,
      };
    case "ADD_ID_ITEM":
      return {
        idItem: action.idItem,
      };
    default:
      return state;
  }
};
