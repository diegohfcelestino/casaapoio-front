import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function addIdItem(id) {
  return {
    type: "ADD_ID_ITEM",
    idItem: id,
  };
}

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const dispatch = store.dispatch;
  const { idItem } = store.getState().ComposicaoAcomodacao;
  const record = {
    idCaLeito: null,
    idCaSubLeito: null,
    ...values,
    idCaComposicaoAcomodacao: null,
    idCaAla: idItem,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CaComposicaoAcomodacao", record)
    .then((res) => dispatch(addIdItem(idItem)))
    .catch((err) => handleError("Erro ao incluir", err));
}

export function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const { idItem } = store.getState().ComposicaoAcomodacao;
  const params = {
    idCaQuarto: 0,
    idCaLeito: 0,
    idCaSubLeito: 0,
    dataInclusao: null,
    idEmpresa: auth.empresa,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaComposicaoAcomodacao/GetPorEmpresa?${qs.stringify({
    ...newParams,
    idCaAla: idItem,
  })}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadAla(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    ala: null,
    dataInclusao: null,
    skip: 0,
    take: 0,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaAla/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadBed(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const id = typeof loadOptions === "number" ? loadOptions : null;
  const params = {
    idCaLeito: id,
    idEmpresa: auth.empresa,
    leito: loadOptions.searchValue,
    dataInclusao: null,
    skip: 0,
    take: 10,
  };
  if (!id) delete params.idCaLeito;
  const url = `/CaLeito/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;

      if (id !== null) {
        return data;
      }

      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadSubGrade(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const id = typeof loadOptions === "number" ? loadOptions : null;
  const params = {
    idCaSubLeito: id,
    idEmpresa: auth.empresa,
    subleito: loadOptions.searchValue,
    dataInclusao: null,
    skip: 0,
    take: 10,
  };
  if (!id) delete params.idCaSubLeito;
  const url = `/CaSubLeito/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;

      if (id !== null) {
        return data;
      }

      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleLoadRoom(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const id = typeof loadOptions === "number" ? loadOptions : null;
  const params = {
    idCaQuarto: id,
    idEmpresa: auth.empresa,
    quarto: loadOptions.searchValue,
    skip: loadOptions.skip,
    take: loadOptions.take,
  };

  if (!id) delete params.idCaQuarto;
  if (!loadOptions.searchValue) delete params.quarto;
  const url = `/CaQuarto/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;

      if (id !== null) {
        return data;
      }

      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(idCaComposicaoAcomodacao) {
  return api
    .delete(`/CaComposicaoAcomodacao/Id/${idCaComposicaoAcomodacao}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export function handleUpdate(params) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const dispatch = store.dispatch;
  const { selected } = store.getState().ComposicaoAcomodacao;
  const url = "/CaComposicaoAcomodacao";
  const record = {
    ...selected,
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
  };

  return api
    .put(url, record)
    .then((res) => dispatch(addIdItem(selected.idCaAla)))
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaComposicaoAcomodacao",
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};

export const gridDataSourceAla = {
  store: new CustomStore({
    key: "idCaAla",
    load: handleLoadAla,
  }),
};

export const gridDataSourceBed = {
  store: new CustomStore({
    key: "idCaLeito",
    byKey: handleLoadBed,
    load: handleLoadBed,
  }),
};

export const gridDataSourceSubGrade = {
  store: new CustomStore({
    key: "idCaLeito",
    byKey: handleLoadSubGrade,
    load: handleLoadSubGrade,
  }),
};

export const gridDataSourceRoom = {
  store: new CustomStore({
    key: "idCaQuarto",
    byKey: handleLoadRoom,
    load: handleLoadRoom,
  }),
  pageSize: 10,
  paginate: true,
};
