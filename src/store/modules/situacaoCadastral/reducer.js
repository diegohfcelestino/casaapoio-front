// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SELECT_ITEM":
      return {
        selected: action.selected,
      };
    default:
      return state;
  }
};
