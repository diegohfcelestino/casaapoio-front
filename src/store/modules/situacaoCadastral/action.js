import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeSituacaoCadastral: 0,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CeSituacaoCadastral", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeSituacaoCadastral/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeSituacaoCadastral }) {
  return api
    .delete(`/CeSituacaoCadastral/Id/${idCeSituacaoCadastral}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selected } = store.getState().Ala;

  return api
    .put("/CeSituacaoCadastral", selected)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCeSituacaoCadastral", "dataInclusao"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
