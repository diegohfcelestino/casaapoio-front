const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_TYPE_CONTACT":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_TYPE_CONTACT":
      return {
        ...state,
        selectedTypeContact: action.selectedTypeContact,
      };
    case "UPDATE_TYPE_CONTACT":
      return initialState;
    case "DELETE_TYPE_CONTACT":
      return initialState;
    case "CLEAN_DATA_TYPE_CONTACT":
      return initialState;
    default:
      return state;
  }
};
