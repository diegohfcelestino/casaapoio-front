// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "TOGGLE_MENU":
      return {
        ...state,
        isOpenMenu: action.isOpenMenu,
      };
    default:
      return state;
  }
};
