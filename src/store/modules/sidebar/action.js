export function toggleMenu(isOpenMenu) {
  return (dispatch) => {
    dispatch({
      type: "TOGGLE_MENU",
      isOpenMenu,
    });
  }
}
