// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SELECT_TREATMENT":
      return {
        selectedTreatment: action.selectedTreatment,
      };
    default:
      return state;
  }
};
