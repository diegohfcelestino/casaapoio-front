import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values, nomeUsuario) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeUsuarioCentro: 0,
    ...values,
    nomeUsuario,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CeUsuarioCentroCusto", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions, nomeUsuario) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeUsuarioCentroCusto/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeUsuarioCentro }, nomeUsuario) {
  const params = {
    id: idCeUsuarioCentro
  }
  const url = `/CeUsuarioCentroCusto/Id/${nomeUsuario}?${qs.stringify(params)}`;

  return api
    .delete(url)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selected } = store.getState().Kit;

  return api
    .put("/CeUsuarioCentroCusto", selected)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export function gridDataSource(nomeUsuario) {
  return {
    store: new CustomStore({
      key: ["idCeUsuarioCentro", "dataInclusao"],
      load: (loadOptions) => handleLoad(loadOptions, nomeUsuario),
      insert: (values) => handleInsert(values, nomeUsuario),
      update: handleUpdate,
      remove: (values) => handleRemove(values, nomeUsuario),
    })
  }
};
