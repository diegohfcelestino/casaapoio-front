import api from "../../../services/api";
import { postAndPut } from "./object";
import { handleErrorForm } from "../../../services/helper";
import { Auth } from "../../../config/storage";

export function addIdItem(id) {
  return {
    type: "ADD_ID_ITEM",
    idItem: id
  };
}

export function selectDetail(selectedDetail) {
  return {
    type: "SELECTED_DETAIL",
    selectedDetail
  };
}

export function createAccommodation(
  form,
  setForm,
  setLoading,
  successMessage,
  errorMessage
) {
  return (dispatch) => {
    const method = form.idCaSolicitaHospedagem ? "put" : "post";
    const action = form.idCaSolicitaHospedagem
      ? "Alteração feita"
      : "Cadastro feito";
    const data = {
      dataInclusao: new Date(),
      ...postAndPut(form)
    };
    const url = "/CaSolicitaHospedagem";
    setLoading(true);

    api({ method, url, data })
      .then((response) => {
        const { data } = response;
        setForm((form) => ({
          ...form,
          idCaSolicitaHospedagem: data.idCaSolicitaHospedagem
        }));
        successMessage(`${action} com sucesso`);
      })
      .catch((err) => {
        errorMessage(handleErrorForm(`Erro no ${action.toLowerCase()}`, err));
      })
      .finally(() => setLoading(false));
  };
}

export function sendLink(form, setLoadingEmail, successMessage) {
  const {
    assistenteSocial = "Assistente Social",
    emailAssistSocial,
    idCaSolicitaHospedagem
  } = form;
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const params = {
    idPessoaEmpresa: auth.empresa,
    idCaSolicitaHospedagem,
    nomeAssistenteSocial: assistenteSocial,
    email: emailAssistSocial.trim()
  };
  setLoadingEmail(true);

  return (dispatch) => {
    api
      .post("/CaSolicitaHospedagem/EnviaEmail", params)
      .then(({ data }) => successMessage(data.message))
      .catch((error) => handleErrorForm(error.response))
      .finally(() => setLoadingEmail(false));
  };
}
