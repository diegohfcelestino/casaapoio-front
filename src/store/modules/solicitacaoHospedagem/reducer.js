// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "ADD_ID_ITEM":
      return {
        ...state,
        idItem: action.idItem
      };
    case "SELECTED_DETAIL":
      return {
        ...state,
        selectedDetail: action.selectedDetail
      };
    default:
      return state;
  }
};
