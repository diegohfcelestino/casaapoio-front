import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import qs from "qs";

export function alterAtenderRequisicaoProduto(
  data,
  successMessage,
  errorMessage
) {
  let form = [];
  data.forEach((element) => {
    form = [...form, element.data];
  });
  api
    .put("CeRequisicaoProdItem", form)
    .then((res) => {
      successMessage("Alteração feita com sucesso");
    })
    .catch((err) => {
      errorMessage(handleErrorForm("Erro ao alterar", err));
    });
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    nomeUsuario: auth?.nomeUsuario,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CeRequisicaoProduto/GetRequisicaoProdCentroCusto?${qs.stringify(
    newParams
  )}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCeRequisicaoProduto",
    load: handleLoad,
  }),
};

export function getPaciente(idCaHospedagem) {
  return (dispatch) => {
    dispatch({
      type: "GET_PACIENTE",
      idCaHospedagem,
    });
  };
}
