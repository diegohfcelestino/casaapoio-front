import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeOperacaoEstoque: 0,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("CeOperacaoEstoque", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    idCeOperacaoEstoque: 0,
    descricao: ""
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeOperacaoEstoque/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeOperacaoEstoque }) {
  return api
    .delete(`/CeOperacaoEstoque/Id/${idCeOperacaoEstoque}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selected } = store.getState().OperacaoEstoque;


  return api
    .put("/CeOperacaoEstoque", selected)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}


function handleLoadTypeGeneric(loadOptions, url) {
  const params = {
    skip: 0,
    take: 1000,
  };
  const urlComplete = `${url}?${qs.stringify(params)}`;

  return api
    .get(urlComplete)
    .then((res) => {
      const { data, totalCount } = res.data;

      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleLoadByKey(key, name, url) {
  const params = {
    [name]: key,
  };
  const newUrl = `${url}${qs.stringify(params)}`;
  return api
    .get(newUrl)
    .then(({ data }) => {
      return [{ ...data }];
    })
    .catch((error) => handleError(error.response));
}

export const gridDataSourceEntradaSaida = {
  store: new CustomStore({
    key: "idCeEntradaSaida",
    byKey: (key, loadOptions) =>
      handleLoadByKey(
        key,
        "idCeEntradaSaida",
        "CeEntradaSaida/GetCeEntradaSaidaById?"
      ),
    load: (loadOptions) =>
      handleLoadTypeGeneric(loadOptions, "CeEntradaSaida/GetCeEntradaSaidaAutoComplete"),
  }),
};

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCeOperacaoEstoque"],
    byKey: (key, loadOptions) =>
      handleLoadByKey(key, "idCaTipoEmail", "CaTipoEmail/GetCaTipoEmailById?"),
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
