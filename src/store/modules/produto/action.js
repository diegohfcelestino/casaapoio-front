import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import apiXHR from "../../../services/apiXHR";
import { Auth } from "../../../config/storage";
import {
  handleError,
  handleParams,
  handleErrorForm,
} from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

export function handleInsert(values, setForm, successMessage) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeProduto: 0,
    ...values,
    kit: values?.kit ? "S" : "N",
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CeProduto", record)
    .then(({ data }) => {
      successMessage("Cadastro feito com sucesso", data?.idCeProduto);
    })
    .catch((err) => handleError("Erro ao incluir", err));
}

export function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeProduto/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleLoadSelect() {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
    skip: 0,
    take: 1000,
  };
  const url = `/CeProduto/GetPorEmpresa?${qs.stringify(params)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export function handleRemove({ idCeProduto }) {
  return api
    .delete(`/CeProduto/Id/${idCeProduto}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export function handleUpdate(values, successMessage) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    ...values,
    kit: values?.kit ? "S" : "N",
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .put("/CeProduto", record)
    .then(({ data }) => {
      successMessage("Alteração feita com sucesso", data?.idCeProduto);
    })
    .catch((err) => handleError("Erro ao alterar", err));
}

export const gridDataSourceSelect = {
  store: new CustomStore({
    key: ["idCeProduto", "dataInclusao"],
    load: handleLoadSelect,
  }),
};

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCeProduto", "dataInclusao"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};

export function addProductId(idCeProduto) {
  return {
    type: "ADD_PRODUCT_ID",
    idCeProduto,
  };
}

export function addImage(foto, setLoadImage, uppy, handleImages) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  const idCeProduto = store.getState().Produto.idCeProduto;

  const formObject = {
    idCeProduto,
    empresa: user.empresa,
    idCaSaidaTemporaria: 0,
    excluido: "F",
    usuario: user.nomeUsuario,
    dataInclusao: now.toDateString(),
  };
  const formData = new FormData();
  formData.append("foto", foto?.data);
  formData.append("ceProdutoImagemVo", JSON.stringify(formObject));

  apiXHR
    .post("CeProdutoImagem", formData)
    .then((res) => {
      setLoadImage(false);
      uppy.cancelAll();
      setTimeout(() => {
        handleImages();
      }, 1000);
    })
    .catch((err) => {
      setLoadImage(false);
      handleErrorForm("Erro ao cadastrar", err);
      uppy.cancelAll();
    });
}

export function getImage() {
  return new Promise((resolve, reject) => {
    const auth = JSON.parse(sessionStorage.getItem(Auth));
    const idCeProduto = store.getState().Produto.idCeProduto;
    const params = {
      idCeProduto,
      idEmpresa: auth?.empresa,
      skip: 0,
      take: 4,
    };
    const url = `CeProdutoImagem/GetPorProduto?${qs.stringify(params)}`;

    return api
      .get(url)
      .then((res) => {
        const { data } = res.data;
        resolve(data);
      })
      .catch((err) => {
        reject(err);
        handleError("Erro ao carregar", err);
      });
  });
}

export function removeImage(id, handleImages) {
  return api
    .delete(`CeProdutoImagem?id=${id}`)
    .then((res) => {
      handleImages();
    })
    .catch((err) => console.error(err));
}
