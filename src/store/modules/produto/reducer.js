// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SELECT_ITEM":
      return {
        selected: action.selected,
      };
    case "ADD_PRODUCT_ID":
      return {
        idCeProduto: action.idCeProduto,
      };
    default:
      return state;
  }
};
