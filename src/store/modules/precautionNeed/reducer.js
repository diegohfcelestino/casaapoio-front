const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_PRECAUTION_NEED":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_PRECAUTION_NEED":
      return {
        ...state,
        selectedPrecautionNeed: action.selectedPrecautionNeed,
      };
    case "UPDATE_PRECAUTION_NEED":
      return initialState;
    case "DELETE_PRECAUTION_NEED":
      return initialState;
    case "CLEAN_DATA_PRECAUTION_NEED":
      return initialState;
    default:
      return state;
  }
};
