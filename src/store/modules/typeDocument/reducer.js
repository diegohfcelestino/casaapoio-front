const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_TYPE_DOCUMENT":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_TYPE_DOCUMENT":
      return {
        ...state,
        selectedTypeDocument: action.selectedTypeDocument,
      };
    case "UPDATE_TYPE_DOCUMENT":
      return initialState;
    case "DELETE_TYPE_DOCUMENT":
      return initialState;
    case "CLEAN_DATA_TYPE_DOCUMENT":
      return initialState;
    default:
      return state;
  }
};
