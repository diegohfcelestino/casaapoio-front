// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SELECT_PRECAUTION_NEED_TYPE":
      return {
        selectedPrecautionNeedType: action.selectedPrecautionNeedType,
      };
    default:
      return state;
  }
};
