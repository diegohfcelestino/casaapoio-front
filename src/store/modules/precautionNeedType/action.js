import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectPrecautionNeedType(selectedPrecautionNeedType) {
  return {
    type: "SELECT_PRECAUTION_NEED_TYPE",
    selectedPrecautionNeedType,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCaTipoNecessidaPrecau: 0,
    ...values,
    empresa: auth.empresa,
    dataInclusao: new Date(),
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CaTipoNecessidaPrecau", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaTipoNecessidaPrecau/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCaTipoNecessidaPrecau }) {
  return api
    .delete(`/CaTipoNecessidaPrecau/Id/${idCaTipoNecessidaPrecau}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selectedPrecautionNeedType } = store.getState().PrecautionNeedType;

  return api
    .put("/CaTipoNecessidaPrecau", selectedPrecautionNeedType)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCaTipoNecessidaPrecau", "dataInclusao"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
