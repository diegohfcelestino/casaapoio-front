import CustomStore from "devextreme/data/custom_store";
import qs from "qs";

import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import {
  handleError,
  filterNonNull,
  handleErrorForm,
  handleParams,
} from "../../../services/helper";

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    cpf:null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaSolicitaHospedagem/GetHospedagemNaoAprovada?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaSolicitaHospedagem",
    load: handleLoad,
  }),
}

export function handleAprovaSolicitacaoHospedagem(id) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const url = `/CaSolicitaHospedagem/AprovaSolicitacaoHospedagem`;
  const data = {
    idCaSolicitaHospedagem:id,
    usuario:auth.nomeUsuario
  }
  return (dispatch) => {
    return api
      .post(url,data)
      .then(({ data }) => {
        return data;
      })
      .catch((err) => handleErrorForm("Erro ao carregar", err));
  };
}