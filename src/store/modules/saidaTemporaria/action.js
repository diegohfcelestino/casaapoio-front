import CustomStore from "devextreme/data/custom_store";
import api from "../../../services/api";
import apiXHR from "../../../services/apiXHR";
import { Auth } from "../../../config/storage";
import {
  handleErrorForm,
  handleError,
  handleParams,
} from "../../../services/helper";
import qs from "qs";

export function createSaidaTemporaria(
  form,
  setForm,
  foto,
  successMessage,
  errorMessage,
) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();

  const formObject = {
    ...form,
    empresa: user.empresa,
    idCaSaidaTemporaria: 0,
    excluido: "F",
    usuario: user.nomeUsuario,
    dataInclusao: now.toDateString(),
  };
  const formData = new FormData();
  formData.append("foto", foto?.data);
  formData.append("caSaidaTemporariaVo", JSON.stringify(formObject));

  apiXHR
    .post("CaSaidaTemporaria", formData)
    .then((res) => {
      successMessage("Cadastro feito com sucesso");
    })
    .catch((err) => {
      errorMessage(handleErrorForm("Erro ao cadastrar", err));
      if (!form?.idCaHospedagem || form?.idCaHospedagem === "") {
        errorMessage(
          "Erro ao cadastrar, selecione um paciente e um responsável",
        );
      }
    });
}

export function alterSaidaTemporaria(form, foto, successMessage, errorMessage) {
  const user = JSON.parse(sessionStorage.getItem(Auth));
  const now = new Date();
  const formObject = {
    ...form,
    empresa: user.empresa,
    excluido: "F",
    usuario: user.nomeUsuario,
    dataInclusao: now.toDateString(),
  };
  const formData = new FormData();
  foto?.data && formData.append("foto", foto?.data);
  formData.append("caSaidaTemporariaVo", JSON.stringify(formObject));

  if (form?.idCaSaidaTemporaria) {
    apiXHR
      .put("CaSaidaTemporaria", formData)
      .then((res) => {
        successMessage("Alteração feita com sucesso");
      })
      .catch((err) => {
        errorMessage(handleErrorForm("Erro ao alterar", err));
      });
  } else {
    errorMessage(
      "SaidaTemporaria não é válido, por favor faça a busca novamente.",
    );
  }
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth?.empresa,
    nome: null,
    dataInclusao: null,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `CaSaidaTemporaria/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove(id) {
  return api
    .delete(`CaSaidaTemporaria/Id/${id}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: "idCaSaidaTemporaria",
    load: handleLoad,
    remove: handleRemove,
  }),
};

export function getPaciente(idCaHospedagem) {
  return (dispatch) => {
    dispatch({
      type: "GET_PACIENTE",
      idCaHospedagem,
    });
  };
}

export function getImage(id, setFoto) {
  api
    .get(`CaSaidaTemporaria/DownloadFotoBase64?${qs.stringify({ id })}`)
    .then(({ data }) => {
      setFoto({
        preview: `data:image/png;base64,${data?.base64}`,
      });
    })
    .catch((err) => {
      console.error(err);
    });
}
