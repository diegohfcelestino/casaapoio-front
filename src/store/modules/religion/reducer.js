const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_RELIGION":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_RELIGION":
      return {
        ...state,
        selectedReligion: action.selectedReligion,
      };
    case "UPDATE_RELIGION":
      return initialState;
    case "DELETE_RELIGION":
      return initialState;
    case "CLEAN_DATA_RELIGION":
      return initialState;
    default:
      return state;
  }
};
