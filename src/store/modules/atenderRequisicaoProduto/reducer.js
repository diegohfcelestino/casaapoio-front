const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "GET_PACIENTE":
      return {
        ...state,
        idCaHospedagem: action.idCaHospedagem,
      };
    default:
      return state;
  }
};
