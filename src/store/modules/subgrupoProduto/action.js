import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values, idGrupoProduto) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeSubGrupoProduto: 0,
    idCeGrupoProduto: idGrupoProduto,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CeSubGrupoProduto", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions, idCeGrupoProduto) {
  const params = {
    idCeGrupoProduto,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeSubGrupoProduto/GetPorIdGrupoProduto?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeSubGrupoProduto }, idGrupoProduto) {
  const params = {
    id: idCeSubGrupoProduto
  }
  const url = `/CeSubGrupoProduto/Id/${idGrupoProduto}?${qs.stringify(params)}`;

  return api
    .delete(url)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selected } = store.getState().SubgrupoProduto;

  return api
    .put("/CeSubGrupoProduto", selected)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export function gridDataSource(idGrupoProduto) {
  return {
    store: new CustomStore({
      key: ["idCeSubGrupoProduto", "dataInclusao"],
      load: (loadOptions) => handleLoad(loadOptions, idGrupoProduto),
      insert: (values) => handleInsert(values, idGrupoProduto),
      update: handleUpdate,
      remove: (values) => handleRemove(values, idGrupoProduto),
    })
  }
};
