const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_SIZE_SHOE":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_SIZE_SHOE":
      return {
        ...state,
        selectedSizeShoe: action.selectedSizeShoe,
      };
    case "UPDATE_SIZE_SHOE":
      return initialState;
    case "DELETE_SIZE_SHOE":
      return initialState;
    case "CLEAN_DATA_SIZE_SHOE":
      return initialState;
    default:
      return state;
  }
};
