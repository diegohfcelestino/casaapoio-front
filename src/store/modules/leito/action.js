import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const date = new Date(); //adicionado variavel provisoria, pois não podemos passar data.
  const record = {
    idCaLeito: 0,
    ...values,
    dataInclusao: date.toISOString(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CaLeito", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const params = {
    idEmpresa: auth.empresa,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CaLeito/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCaLeito }) {
  return api
    .delete(`/CaLeito/Id/${idCaLeito}`)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate(key, values) {
  const { selected } = store.getState().Ala;

  return api
    .put("/CaLeito", selected)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}

export const gridDataSource = {
  store: new CustomStore({
    key: ["idCaLeito", "dataInclusao"],
    load: handleLoad,
    insert: handleInsert,
    update: handleUpdate,
    remove: handleRemove,
  }),
};
