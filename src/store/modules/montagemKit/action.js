import qs from "qs";
import CustomStore from "devextreme/data/custom_store";

import store from "../../Store";
import api from "../../../services/api";
import { Auth } from "../../../config/storage";
import { handleError, handleParams } from "../../../services/helper";

export function selectItem(selected) {
  return {
    type: "SELECT_ITEM",
    selected,
  };
}

function handleInsert(values, idCeKit) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));
  const record = {
    idCeKitProduto: 0,
    idCeKit,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  };

  return api
    .post("/CeKitProduto", record)
    .then((res) => res)
    .catch((err) => handleError("Erro ao incluir", err));
}

function handleLoad(loadOptions, idCeKit) {
  const params = {
    idCeKit,
    descricao: "",
    dataInclusao: "",
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeKitProduto/GetPorIdCeKit?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemove({ idCeKitProduto }) {
  const params = {
    id: idCeKitProduto
  }
  const url = `/CeKitProduto/Id/${idCeKitProduto}?${qs.stringify(params)}`;

  return api
    .delete(url)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}

function handleUpdate({ idCeKitProduto }, values, idCeKit) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const data = {
    idCeKitProduto,
    idCeKit,
    ...values,
    dataInclusao: new Date(),
    empresa: auth.empresa,
    usuario: auth.nomeUsuario,
    excluido: "F",
  }

  return api
    .put("/CeKitProduto", data)
    .then((res) => res)
    .catch((err) => handleError("Erro ao editar", err));
}



function handleLoadKit(loadOptions) {
  const auth = JSON.parse(sessionStorage.getItem(Auth));

  const params = {
    idCeProduto: 0,
    descricaoProduto: "",
    dataInclusao: "",
    idEmpresa: auth.empresa,
  };
  const newParams = handleParams(params, loadOptions);
  const url = `/CeKit/GetPorEmpresa?${qs.stringify(newParams)}`;

  return api
    .get(url)
    .then((res) => {
      const { data, totalCount } = res.data;
      return {
        data,
        totalCount,
      };
    })
    .catch((err) => handleError("Erro ao carregar", err));
}

function handleRemoveKit({ idCeKit }) {
  const params = {
    id: idCeKit
  }
  const url = `/CeKit/Id/${idCeKit}?${qs.stringify(params)}`;

  return api
    .delete(url)
    .then((res) => res)
    .catch((err) => handleError("Erro ao remover", err));
}



export const gridDataSourceKit = {
  store: new CustomStore({
    key: ["idCeKit", "dataInclusao"],
    load: handleLoadKit,
    remove: handleRemoveKit
  })

};

export function gridDataSource(idCeKit) {
  return {
    store: new CustomStore({
      key: ["idCeKitProduto", "dataInclusao"],
      load: (loadOptions) => handleLoad(loadOptions, idCeKit),
      insert: (values) => handleInsert(values, idCeKit),
      update: (key, values) => handleUpdate(key, values, idCeKit),
      remove: (values) => handleRemove(values, idCeKit),
    })
  }
};
