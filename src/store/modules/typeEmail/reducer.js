const initialState = {};
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = {}, action) => {
  switch (action.type) {
    case "SEARCH_FILTER_TYPE_EMAIL":
      return {
        ...state,
        filterRegisters: action.filterRegisters,
      };
    case "SELECT_TYPE_EMAIL":
      return {
        ...state,
        selectedTypeEmail: action.selectedTypeEmail,
      };
    case "UPDATE_TYPE_EMAIL":
      return initialState;
    case "DELETE_TYPE_EMAIL":
      return initialState;
    case "CLEAN_DATA_TYPE_EMAIL":
      return initialState;
    default:
      return state;
  }
};
