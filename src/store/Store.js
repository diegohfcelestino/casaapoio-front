import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootreducers from "./modules/rootReducer";

const middleware = applyMiddleware(thunk);

const store = createStore(rootreducers, composeWithDevTools(middleware));

export default store;