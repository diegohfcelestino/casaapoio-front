import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { Auth } from "./config/storage";

import Login from "./pages/login/Login";
import Sidebar from "./components/sidebar/Sidebar";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Pages from "./authorization/Pages";
import RouterAuthorization from "./authorization/RouterAuthorization";

const PrivateRoute = ({ component: Component, setTheme, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      sessionStorage.getItem(Auth) != null ? (
        <div style={{ display: "flex", flexWrap: "nowrap" }}>
          <Sidebar {...props} />

          <div
            className={`routerContainer ${
              rest.isOpen ? "menuOpen" : "menuClose"
            }`}
          >
            <Header setTheme={setTheme} />
            <div style={{ minHeight: "80vh" }}>
              <Component {...props} />
            </div>
            <Footer />
          </div>
        </div>
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

const GuestRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        !sessionStorage.getItem(Auth) ? (
          <Component {...rest} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

const Routes = (props) => {
  return (
    <Switch>
      <GuestRoute path="/login" component={Login} />
      {Pages.map((page) => {
        return (
          <PrivateRoute
            exact
            key={page.route}
            path={page.route}
            component={RouterAuthorization(page.component, page.name)}
            setTheme={props.setTheme}
          />
        );
      })}
    </Switch>
  );
};

export default Routes;
